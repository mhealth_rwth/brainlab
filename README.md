# BrainLab

BrainLab is an android application that is ued to connect to the [EMOTIV EPOC+ 14-Channel Wireless EEG Headset](https://www.emotiv.com/epoc/) and record EEG signal data with use of the EEG devices bluetooth dongle. Additionally, experiments can be specified. The cognitive tasks can be performed while recording.