package mhealth.eeg_presentation_app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import java.util.HashMap;

import mhealth.eeg_presentation_app.DemoViewer.DemoMeasuring;
import mhealth.eeg_presentation_app.StimuliViewer.ExperimentOverview;


public class SetUp extends AppCompatActivity {
    private boolean inBackground = false;
    private boolean disconnected = false;
    // BroadcastReceiver
    private static final String ACTION_STRING_OVERVIEW = "ToOverview";
    private static final String ACTION_STRING_ACTIVITY = "ToAll";
    private static int battery_in_percent = 0;
    private static double battery_time_left = 0;
    private static String serial_number = "";
    private static String model = "";

    private static boolean activated = true;

    // Array with all electrode names
    private static String[] electrodes = {"F3", "FC5", "AF3", "F7", "T7", "P7", "O1", "O2", "P8", "T8", "F8", "AF4", "FC6", "F4"};
    // Hashmap with names & signal qualities of the electrodes
    private static HashMap<String, Integer> electrode_quality = new HashMap<>();

    /**
     * register to BroadcastReceivers of the emotivService to get data
     * start thread for updating status
     *
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("SetUp", "onCreate() called");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_over_view);
        activated = true;

        // set initial images for the displaying of the electrode signal qualities by using bitmaps
        setImage("head", "head", 800,800);
        setImage("battery_status_icon", "battery_0",140,40);
        for (String electrode : electrodes) {
            setImage(electrode.toLowerCase(), electrode.toLowerCase() + "_grey",90,90);
        }

        if (decodedDataReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_OVERVIEW);
            registerReceiver(decodedDataReceiver, intentFilter);
        }
        if (toAllReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_ACTIVITY);
            registerReceiver(toAllReceiver, intentFilter);
        }

        new startUpdateThread().start();
    }

    /**
     * BroadcastReceiver to detect when EEG has been disconnected
     */
    private BroadcastReceiver toAllReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent main_intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(main_intent);
            unregisterReceiver(toAllReceiver);
        }
    };

    /**
     * BroadcastReceiver to receive status data of the EEG
     */
    private BroadcastReceiver decodedDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // read status data out of the received bundle & save it
            Bundle intent_extras = intent.getExtras();
            battery_in_percent = intent_extras.getInt("battery_in_percent");
            battery_time_left = intent_extras.getDouble("battery_time_left");
            serial_number = intent_extras.getString("serial");
            model = intent_extras.getString("model");
            // signal qualities of the electrodes
            for (String electrode : electrodes) {
                int contactQuality = intent_extras.getInt(electrode + "_quality");
                int quality;
                if (contactQuality < 81) { // < 81
                    quality = -1; //SensorManager.SENSOR_STATUS_NO_CONTACT
                }
                else if (contactQuality < 221) { // < 221
                    quality = SensorManager.SENSOR_STATUS_UNRELIABLE;
                }
                else if (contactQuality < 314) { // < 314
                    quality = SensorManager.SENSOR_STATUS_ACCURACY_LOW;
                }
                else if (contactQuality < 407) { // < 407
                    quality = SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM;
                }
                else {
                    // >= 407
                    quality = SensorManager.SENSOR_STATUS_ACCURACY_HIGH;
                }
                electrode_quality.put(electrode, quality);
            }
        }
    };

    /**
     * Thread for updating the status information
     */
    private class startUpdateThread extends Thread {
        public void run() {
            while (activated) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        for (HashMap.Entry<String, Integer> electrode : electrode_quality.entrySet()) {
                            // depending on signal quality, get ID of respective electrode with according color and set View to it
                            switch (electrode.getValue()) {
                                case 0:
                                    setImage(electrode.getKey().toLowerCase(), electrode.getKey().toLowerCase() + "_red",90,90);
                                    break;
                                case 1:
                                    setImage(electrode.getKey().toLowerCase(), electrode.getKey().toLowerCase() + "_orange",90,90);
                                    break;
                                case 2:
                                    setImage(electrode.getKey().toLowerCase(), electrode.getKey().toLowerCase() + "_yellow",90,90);
                                    break;
                                case 3:
                                    setImage(electrode.getKey().toLowerCase(), electrode.getKey().toLowerCase() + "_green",90,90);
                                    break;
                                default:
                                    setImage(electrode.getKey().toLowerCase(), electrode.getKey().toLowerCase() + "_grey",90,90);
                            }
                        }

                        // update also battery information
                        TextView battery_percent = (TextView) findViewById(R.id.battery_status); // battery percent
                        TextView battery_left = (TextView) findViewById(R.id.battery_left); // battery time left

                        // update everything according to the charge in percent
                        if (battery_in_percent == 0) {
                            battery_percent.setText(Integer.toString(battery_in_percent) + " %");
                            battery_left.setText(Double.toString(battery_time_left) + " hours left");
                            //  battery_image.setImageResource(R.drawable.battery0);
                            setImage("battery_status_icon", "battery_0",140,40);
                        } else if (battery_in_percent < 10) {
                            battery_percent.setText(Integer.toString(battery_in_percent) + " %");
                            battery_left.setText(Double.toString(battery_time_left) + " hours left");
                            //  battery_image.setImageResource(R.drawable.battery5);
                            setImage("battery_status_icon", "battery_5",140,40);
                        } else if (battery_in_percent < 20) {
                            battery_percent.setText(Integer.toString(battery_in_percent) + " %");
                            battery_left.setText(Double.toString(battery_time_left) + " hours left");
                            //  battery_image.setImageResource(R.drawable.battery10);
                            setImage("battery_status_icon", "battery_10",140,40);
                        } else if (battery_in_percent < 30) {
                            battery_percent.setText(Integer.toString(battery_in_percent) + " %");
                            battery_left.setText(Double.toString(battery_time_left) + " hours left");
                            //  battery_image.setImageResource(R.drawable.battery20);
                            setImage("battery_status_icon", "battery_20",140,40);
                        } else if (battery_in_percent < 50) {
                            battery_percent.setText(Integer.toString(battery_in_percent) + " %");
                            battery_left.setText(Double.toString(battery_time_left) + " hours left");
                            //  battery_image.setImageResource(R.drawable.battery40);
                            setImage("battery_status_icon", "battery_40",140,40);
                        } else if (battery_in_percent < 70) {
                            battery_percent.setText(Integer.toString(battery_in_percent) + " %");
                            battery_left.setText(Double.toString(battery_time_left) + " hours left");
                            //  battery_image.setImageResource(R.drawable.battery60);
                            setImage("battery_status_icon", "battery_60",140,40);
                        } else if (battery_in_percent < 90) {
                            battery_percent.setText(Integer.toString(battery_in_percent) + " %");
                            battery_left.setText(Double.toString(battery_time_left) + " hours left");
                            // battery_image.setImageResource(R.drawable.battery80);
                            setImage("battery_status_icon", "battery_80",140,40);
                        } else {
                            battery_percent.setText(Integer.toString(battery_in_percent) + " %");
                            battery_left.setText(Double.toString(battery_time_left) + " hours left");
                            // battery_image.setImageResource(R.drawable.battery100);
                            setImage("battery_status_icon", "battery_100",140,40);
                        }
                    }
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * Start demo measuring if the according button is pressed
     *
     * @param view
     */
    public void goToDemo(View view) {
        Intent intent = new Intent(this, DemoMeasuring.class);
        startActivity(intent);
    }

    /**
     * Start ExperimentOverview if the according button is pressed
     *
     * @param view
     */
    public void goToViewer(View view) {
        Log.d("SetUp", "goToViewer() called");
        Intent intent = new Intent(this, ExperimentOverview.class);
        startActivity(intent);
    }

    /**
     * Set image to imageview either by taking it from mMemoryCache when already existing or creating new bitmap
     *
     * @param view_name  name of ImageView (not ID)
     * @param image_name name of image source (not ID)
     */
    public void setImage(String view_name, String image_name, int width, int height) {
        String imageView_name = view_name;
        int resId = getResources().getIdentifier(imageView_name, "id", getPackageName());
        // get respective view and image id
        ImageView view = (ImageView) findViewById(resId);
        int drawId = getResources().getIdentifier(image_name, "drawable", getPackageName());

        Picasso.with(getApplicationContext()).load(drawId).resize(width, height).into(view);
    }

    /**
     * When activity is destroyed, unregister als BroadcastReceivers
     */
    public void onDestroy() {
        Log.d("SetUp", "onDestroy() called");
        super.onDestroy();
        activated = false;
        try {
            unregisterReceiver(toAllReceiver);
        } catch (Exception e) {

        }
        unregisterReceiver(decodedDataReceiver);
    }

    public void onPause() {
        super.onPause();
    }

    /**
     * When service stops, unregister all Receivers
     */
    public void onStop() {
        Log.d("SetUp", "onStop() called");
        super.onStop();
        inBackground = true;
        activated = false;
    }

    /**
     * When service is restarted, register all Receivers and start updating
     */
    public void onResume() {
        Log.d("SetUp", "onResume() called");
        super.onResume();

        if (decodedDataReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_OVERVIEW);
            registerReceiver(decodedDataReceiver, intentFilter);
        }
        if (toAllReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_ACTIVITY);
            registerReceiver(toAllReceiver, intentFilter);
        }
        activated = true;
        new startUpdateThread().start();

    }

    public void goToHomeScreen() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        goToHomeScreen();
        return true;
    }

    @Override
    public void onBackPressed() {
        boolean allowBackButton = false;
        if (allowBackButton) {
            super.onBackPressed();
        } else {
            goToHomeScreen();
        }
    }

/*
    */
/**
     * When service is restarted, register all Receivers and start updating
     *//*

    public void onRestart() {
        super.onRestart();

        if (decodedDataReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_OVERVIEW);
            registerReceiver(decodedDataReceiver, intentFilter);
        }
        if (toAllReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_ACTIVITY);
            registerReceiver(toAllReceiver, intentFilter);
        }
        activated = true;
        new startUpdateThread().start();
        super.onStart();
        }
*/


}