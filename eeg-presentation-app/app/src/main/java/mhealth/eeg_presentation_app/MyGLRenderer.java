package mhealth.eeg_presentation_app;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.opengl.GLES10;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.os.SystemClock;
import android.view.Display;
import android.view.WindowManager;

/**
 * Created by Ina on 21/03/16.
 */
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class MyGLRenderer implements GLSurfaceView.Renderer {
    private Square square;
    private Context context;
    private Bitmap bitmap;
    private boolean show = false;
    private int duration = 0;
    private Long nextStop = 0L;
    private String label = "";
    /** The texture pointer */
    private int[] textures = new int[1];

    private float swidth;
    private float sheight;
    private float bwidth;
    private float bheight;
    private float xscale;
    private float yscale;
    private float ztranslation;

    // Background color LTGRAY
    private final long backgroundColor =  0xffcccccc;
    private float bAlpha = (float)(backgroundColor >> 24) / 0xFF;
    private float bRed = (float)((backgroundColor >> 16) & 0xFF) / 0xFF;
    private float bGreen = (float)((backgroundColor >> 8) & 0xFF) / 0xFF;
    private float bBlue = (float)(backgroundColor & 0xFF) / 0xFF;


    /**
     *  Broadcast to RecordService with annotations
     */
    protected void sendAnnotationBroadcast(long timestamp, String event) {
        Intent intent_recorder = new Intent();
        intent_recorder.putExtra("timestamp",timestamp);
        intent_recorder.putExtra("event",event);
        intent_recorder.setAction(ACTION_STRING_EVENT);
        this.context.sendBroadcast(intent_recorder);
    }
    /**
     *  Broadcast to RecordService with annotations
     */
    protected void sendAnnotationBroadcast(long timestamp, String event, int duration) {
        Intent intent_recorder = new Intent();
        intent_recorder.putExtra("timestamp",timestamp);
        intent_recorder.putExtra("event",event);
        intent_recorder.putExtra("duration",duration);
        intent_recorder.setAction(ACTION_STRING_EVENT);
        this.context.sendBroadcast(intent_recorder);
    }

    private static final String ACTION_STRING_EVENT = "Event";


    public MyGLRenderer(Context context) {
        this.square = new Square();
        this.context = context;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point screensize = new Point();
        display.getSize(screensize);
        this.swidth = (float)screensize.x;
        this.sheight = (float)screensize.y;
        if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            this.ztranslation = -3.4f;
        } else {
            this.ztranslation = -2.7f;
        }
    }

    public void setDuration(int dur) {
        this.duration = dur;

    }
    public void setBitmap(Bitmap bitmap, String label) {
        if(this.bitmap != null) {
            synchronized (this.bitmap) {
                this.bitmap = bitmap;
                this.label = label;
            }
        } else {
            this.bitmap = bitmap;
            this.label = label;
        }
        this.bwidth = bitmap.getWidth();
        this.bheight = bitmap.getHeight();
        if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            if (this.bwidth >= this.bheight) {
                if (this.bwidth <= this.swidth) {
                    this.xscale = this.bwidth / this.swidth;
                } else {
                    this.xscale = this.swidth / this.bwidth;
                }
                this.yscale = (this.bheight / this.bwidth) * this.xscale;
            } else {
                if (this.bheight < this.sheight) {
                    if(bwidth < swidth) {
                        this.xscale = this.bwidth / this.swidth;
                        this.yscale = (this.bheight / this.bwidth) * this.xscale;
                    } else {
                        this.xscale = this.swidth / this.bwidth;
                        this.yscale = (this.bheight / this.bwidth) * this.xscale;
                    }
                } else {
                    this.xscale = this.sheight / this.bheight;
                    this.yscale = (this.bwidth / this.bheight) * this.xscale;
                }
            }
        } else {
            if (this.bheight >= this.bwidth) {
                if (this.bheight <= this.sheight) {
                    this.yscale = this.bheight / this.sheight;
                } else {
                    this.yscale = this.sheight / this.bheight;
                }
                this.xscale = (this.bwidth / this.bheight) * this.yscale;
            } else {
                if (this.bwidth < this.swidth) {
                    if(this.bheight < this.sheight) {
                        this.yscale = this.bheight / this.sheight;
                        this.xscale = (this.bwidth / this.bheight) * this.yscale;
                    } else {
                        this.yscale = this.sheight / this.bheight;
                        this.xscale = (this.bwidth / this.bheight) * this.yscale;
                    }
                } else {
                    this.yscale = this.swidth / this.bwidth;
                    this.xscale = (this.bheight / this.bwidth) * this.yscale;
                }
            }
        }
        this.show = true;
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
// Load the texture for the square
        //square.loadGLTexture(gl, this.context);

        gl.glEnable(GL10.GL_TEXTURE_2D);			//Enable Texture Mapping ( NEW )
        gl.glShadeModel(GL10.GL_SMOOTH); 			//Enable Smooth Shading
        // gl.glClearColor(0.0f, 0.0f, 0.0f, 1f); 	//Black Background
        gl.glClearColor(bRed, bGreen, bBlue, bAlpha); 	//Set background
        gl.glClearDepthf(1.0f); 					//Depth Buffer Setup
        gl.glEnable(GL10.GL_DEPTH_TEST); 			//Enables Depth Testing
        gl.glDepthFunc(GL10.GL_LEQUAL); 			//The Type Of Depth Testing To Do

        //Really Nice Perspective Calculations
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);

        gl.glClear(GLES10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
    }


    public void onDrawFrame(GL10 gl) {

        if(this.bitmap == null || (nextStop != 0L && System.currentTimeMillis() > nextStop)) {
            show = false;
        }

        // Use Android GLUtils to specify a two-dimensional texture image from our bitmap

        if (this.show && !bitmap.isRecycled()) {
            synchronized (this.bitmap) {
                gl.glDeleteTextures(textures.length, textures, 0);
                gl.glGenTextures(1, textures, 0);
                // ...and bind it to our array
                gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

                // create nearest filtered texture
                gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
                gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

                GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, this.bitmap, 0);
                // Clean up
                this.bitmap.recycle();
            }
            gl.glClear(GLES20.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
            gl.glLoadIdentity();
            // changing z value = zoom
            gl.glTranslatef(0.0f, 0.0f, ztranslation);
            gl.glScalef(this.xscale, this.yscale, 1f);
            Long curTime = System.currentTimeMillis();
            sendAnnotationBroadcast(curTime, label);
            square.draw(gl);
            nextStop = curTime+duration;

        } else if(!this.show){
            gl.glClear(GLES10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
            nextStop = 0L;
        } else {
            gl.glClear(GLES20.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
            gl.glLoadIdentity();
            // changing z value = zoom
            gl.glTranslatef(0.0f, 0.0f, ztranslation);
            gl.glScalef(this.xscale, this.yscale, 1f);
            square.draw(gl);
        }
        gl.glFinish();
    }
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        if(height == 0) {                       //Prevent A Divide By Zero By
            height = 1;                         //Making Height Equal One
        }
        gl.glViewport(0, 0, width, height);     //Reset The Current Viewport
        gl.glMatrixMode(GL10.GL_PROJECTION);    //Select The Projection Matrix
        gl.glLoadIdentity();                    //Reset The Projection Matrix
        //Calculate The Aspect Ratio Of The Window
        GLU.gluPerspective(gl, 45.0f, (float) width / (float) height, 0.1f, 100.0f);
        gl.glMatrixMode(GL10.GL_MODELVIEW);     //Select The Modelview Matrix
        gl.glLoadIdentity();
    }

    public class Square{
        private FloatBuffer vertexBuffer;
        private float vertices[] = {
                -1.0f, -1.0f, 0.0f,
                -1.0f, 1.0f, 0.0f,
                1.0f, -1.0f, 0.0f,
                1.0f, 1.0f, 0.0f
        };

        private FloatBuffer textureBuffer;
        private float texture[] = {
                0.0f, 1.0f, // top left V2
                0.0f, 0.0f, // bottom left V1
                1.0f, 1.0f, // top right V4
                1.0f, 0,0f // bottom right V3
        };

        public Square() {
            // a float has 4 bytes so we allocate for each coordinate 4 bytes
            ByteBuffer vertexByteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
            vertexByteBuffer.order(ByteOrder.nativeOrder());
            // allocates the memory from the byte buffer
            vertexBuffer = vertexByteBuffer.asFloatBuffer();
            // fill the vertexBuffer with the vertices
            vertexBuffer.put(vertices);
            // set the cursor position to the beginning of the buffer
            vertexBuffer.position(0);

            ByteBuffer byteBuffer = ByteBuffer.allocateDirect(vertices.length * 4);
            byteBuffer.order(ByteOrder.nativeOrder());
            vertexBuffer = byteBuffer.asFloatBuffer();
            vertexBuffer.put(vertices);
            vertexBuffer.position(0);

            byteBuffer = ByteBuffer.allocateDirect(texture.length * 4);
            byteBuffer.order(ByteOrder.nativeOrder());
            textureBuffer = byteBuffer.asFloatBuffer();
            textureBuffer.put(texture);
            textureBuffer.position(0);

        }

        public void loadGLTexture(GL10 gl, Context context) {
            // loading texture
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.emotiv_eeg);

            // generate one texture pointer
            gl.glGenTextures(1, textures, 0);
            // ...and bind it to our array
            gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

            // create nearest filtered texture
            gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
            gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

            // Use Android GLUtils to specify a two-dimensional texture image from our bitmap
            GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);

            // Clean up
            bitmap.recycle();
        }

        /** The draw method for the square with the GL context */
        public void draw(GL10 gl) {
            // bind the previously generated texture
            gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);

            // Point to our buffers
            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

            // Set the face rotation
            gl.glFrontFace(GL10.GL_CW);

            // Point to our vertex buffer
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
            gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);

            // Draw the vertices as triangle strip
            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.length / 3);

            //Disable the client state before leaving
            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
            gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        }
    }

}
