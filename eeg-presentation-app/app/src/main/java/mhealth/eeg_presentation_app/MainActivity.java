package mhealth.eeg_presentation_app;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Manages initial connection to and detection of EEG & shows options for use
 */
public class MainActivity extends AppCompatActivity {

    private static final String ACTION_STRING_ACTIVITY = "ToAll";

    public boolean device_connected = false; // Emotiv EPOC dongle already connected?
    public int list_size;
    public int element_number;

    // creates activity
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("MainActivity", "onCreate() called");
        super.onCreate(savedInstanceState);
        // sets layout
        setContentView(R.layout.activity_main);

        // Logging
        Date now = Calendar.getInstance().getTime();
        String nowString = DateFormat.getDateInstance(DateFormat.MEDIUM).format(now) + "_" + DateFormat.getTimeInstance(DateFormat.MEDIUM).format(now);
        String fileName = "eeg_" + nowString;
        fileName = fileName.replaceAll(" ", "_");
        File logDirectory = new File ("storage/emulated/0/Documents/logs");  // "storage/emulated/0/Documents/logs"
        // create log folder
        if ( !logDirectory.exists() ) {
            logDirectory.mkdir();
        }
        File logFile = new File(logDirectory, "logcat" + System.currentTimeMillis() + ".txt" );
        // clear the previous logcat and then write the new one to the file
        try {
            Process process = Runtime.getRuntime().exec("logcat -c");
            process = Runtime.getRuntime().exec("logcat -f " + logFile);
        } catch ( IOException e ) {
            e.printStackTrace();
        }

        setImage("EEG_pic", "emotiv_eeg");

        if (toAllReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_ACTIVITY);
            registerReceiver(toAllReceiver, intentFilter);
        }

        Log.w("START", "test message");
    }

    /**
     * BroadcastReceiver to detect when EEG has been disconnected
     */
    private BroadcastReceiver toAllReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            device_connected = false; // Emotiv EPOC dongle already connected?
            list_size = 0;
            element_number = 0;
            System.out.println("Broadcast to all");
            if(intent.getExtras().getBoolean("TurnedOff")) {
                Toast.makeText(getApplicationContext(),"EEG is turned off",Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(),"EEG USB dongle is disconnected",Toast.LENGTH_LONG).show();
            }
        }
    };

    /**
     * Checks whether a USB device is plugged in and connects if it's an Emotiv EPOC USB dongle
     * Called when "connect" button is pressed
     */
    public void connectEEG(View view) {
        // get USB manager and list of all plugged in devices
        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        UsbDevice device = null;

        device_connected = false;
        list_size = deviceList.size();
        element_number = 0;
        // if no devices plugged in, ask for plugging in Emotiv EPOC dongle
        if(deviceList.size() == 0) {
            Toast.makeText(getApplicationContext(),"Please plug in EEG dongle",Toast.LENGTH_LONG).show();
        } else {
            // iterate over device list, ask for permission to connect each and check whether one is a Emotiv EPOC dongle
            Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
            while (deviceIterator.hasNext() && device_connected == false) {
                device = deviceIterator.next();
                element_number++;
                // ask for permission for connection and check if EMOTIV device
                UsbManager mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
                PendingIntent mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
                IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
                registerReceiver(mUsbReceiver, filter);
                mUsbManager.requestPermission(device, mPermissionIntent);

            }
        }
    }

    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";

    /**
     * BroadcastReceiver for getting permission to connect to a USB device
     */
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if(ACTION_USB_PERMISSION.equals(action)) {
                    synchronized(this) {
                        UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                        if(intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                            if(device != null) { // if permission for found USB device granted

                                // check whether device is Emotiv EPOC dongle
                                int product_id = device.getProductId();
                                int vendor_id = device.getVendorId();
                                int emotiv_vendor_id1 = 4660;
                                int emotiv_product_id1 = 60674;
                                int emotiv_vendor_id2 = 8609;
                                int emotiv_product_id2 = 1;

                                // if Emotiv EPOC dongle, start EmotivService & go to SetUp Activity!
                                if ((product_id == emotiv_product_id1 && vendor_id == emotiv_vendor_id1) || (product_id == emotiv_product_id2 && vendor_id == emotiv_vendor_id2)) {

                                    device_connected = true;
                                    // start service
                                    Intent service_intent = new Intent(getApplicationContext(), EmotivService.class);
                                    service_intent.putExtra(UsbManager.EXTRA_DEVICE, device);
                                    startService(service_intent);

                                    onStop();
                                    // start overview activity

                                } else { // if not Emotiv EPOC dongle, ask for plugging it in
                                    if(list_size == element_number && device_connected == false) {
                                            Toast.makeText(getApplicationContext(), "No Emotiv EEG found! Plug in an Emotiv EPOC dongle!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        } else { // Permission not granted!
                            Toast.makeText(getApplicationContext(),"Permission denied",Toast.LENGTH_LONG).show();
                        }

                    }
                }
            }
    };



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onStop() {
        Log.d("MainActivity", "onStop() called");
        super.onStop();
    }

    public void onDestroy() {
        Log.d("MainActivity", "onDestroy() called");
        super.onDestroy();
        try{
            unregisterReceiver(mUsbReceiver);
            unregisterReceiver(toAllReceiver);
        } catch(IllegalArgumentException e) {

        }
    }

/*
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        registerReceiver(mUsbReceiver, filter);
    }
*/

    protected void onStart() {
        super.onStart();
    }


    /**
     * Set image to imageview either by taking it from mMemoryCache when already existing or creating new bitmap
     * @param view_name name of ImageView (not ID)
     * @param image_name name of image source (not ID)
     */
    public void setImage(String view_name, String image_name) {
        String imageView_name = view_name;
        int resId = getResources().getIdentifier(imageView_name, "id", getPackageName());
        // get respective view and image id
        ImageView view = (ImageView) findViewById(resId);
        int drawId = getResources().getIdentifier(image_name, "drawable", getPackageName());

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayWidth = size.x;

        Picasso.with(getApplicationContext()).load(drawId).resize(800,800).into(view);
    }

}
