package mhealth.eeg_presentation_app;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.widget.Toast;

import mhealth.eeg_presentation_app.Driver.EmotivDevice;

public class EmotivService extends Service {

    boolean device_connected = false;  // variable to save device connection state
    boolean device_turnedOn = true;
    Message decodedData = new Message(); // decoded data packet
    private UsbDevice usbDevice; // usb device (emotiv EEG dongle)

    private static final String ACTION_STRING_ACTIVITY = "ToAll"; // Broadcast to all registered activities
    private static final String ACTION_STRING_OVERVIEW = "ToOverview"; // Broadcast to SetUp activity
    private static final String ACTION_STRING_MEASURING = "ToMeasuringUsers"; // Broadcast to DemoMeasuring activity

    // Names of all electrodes
    private String[] electrodes = {"F3","FC5","AF3","F7","T7","P7","O1","O2","P8","T8","F8","AF4","FC6","F4"};

    int last = -3;

    Intent setup_intent = new Intent();

    // BroadcastReceiver to detect whether device was disconnected
    final BroadcastReceiver detached_receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(intent.getAction())) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null && device.equals(usbDevice)) {
                    device_connected = false;
                    // go back to main activity
                    sendBroadcastToAll(false);
                    // stop service
                    stopSelf();
                }
            }
        }
    };

    public EmotivService() {
    }

    /* decoded data contains:
            <electrodeName> (value of respective electrode)
            <electrodeName>_q (quality of respective electrode)
            received_elapsed_nanos
            received_time
            battery
            battery_left
            gyroX
            gyroY
            counter
            isSync
            serial
            model
     */

    /**
     *  Broadcast to SetUp Activity with specific data
     */
    private void sendBroadcastToSetUp() {
        Intent intent_overview = new Intent();
        intent_overview.putExtra("battery_in_percent",decodedData.getData().getInt("battery")); // Battery percent
        intent_overview.putExtra("battery_time_left",decodedData.getData().getDouble("battery_left")); // left battery time
        intent_overview.putExtra("serial",decodedData.getData().getString("serial")); // serial number
        intent_overview.putExtra("model",decodedData.getData().getString("model")); // model number
        for(int i=0; i<14;i++) { // quality of all electrodes
            intent_overview.putExtra(electrodes[i]+"_quality",decodedData.getData().getInt(electrodes[i]+"_q"));
        }
        intent_overview.setAction(ACTION_STRING_OVERVIEW);
        sendBroadcast(intent_overview);
    }

    /**
     * Broadcast to DemoMeasuring Activity with specific data
     */
    private void sendBroadcastMeasurements() {
        Intent intent_demo = new Intent();
        intent_demo.putExtra("battery_in_percent",decodedData.getData().getInt("battery")); // Battery percent
        intent_demo.putExtra("battery_time_left",decodedData.getData().getDouble("battery_left")); // left battery time
        intent_demo.putExtra("received_time", decodedData.getData().getLong("received_time")); // left battery time
        for(int i=0; i<14;i++) { // values of the electrodes (measurements)
            intent_demo.putExtra(electrodes[i],decodedData.getData().getInt(electrodes[i]));
        }
        for(int i=0; i<14;i++) { // quality of all electrodes
            intent_demo.putExtra(electrodes[i]+"_quality",decodedData.getData().getInt(electrodes[i]+"_q"));
        }
        intent_demo.setAction(ACTION_STRING_MEASURING);
        sendBroadcast(intent_demo);
    }

    /**
     * Broadcast to all Activities which need EEG connection
     * Information that connection is no longer existing
     */
    private void sendBroadcastToAll(Boolean turnedOff) {
        Intent intent_all = new Intent();
        intent_all.putExtra("TurnedOff",turnedOff);
        intent_all.setAction(ACTION_STRING_ACTIVITY);
        sendBroadcast(intent_all);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Connection to EEG in background, receiving of EEG data packets, handles disconnection of EEG
     */
    Boolean started = false;
    int offCounter = 0;
    int lastCounter = 0;

    EmotivDevice emotivDevice = null;
    public int onStartCommand(Intent intent, int flags, final int startId) {
            super.onStartCommand(intent,flags,startId);
            if(intent != null && intent.hasExtra(UsbManager.EXTRA_DEVICE)) {

                setup_intent.setClass(this, SetUp.class);
                setup_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                // get EEG device & show Toast that it was found
                usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                UsbManager usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

                //generate new object of type emotivDevice
                emotivDevice = new EmotivDevice(usbDevice, usbManager, getApplicationContext());

                device_connected = true;
                device_turnedOn = true;

                // Broadcastreceiver to detect if device was disconnected
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);

                registerReceiver(detached_receiver, intentFilter);

                // Messenger which saves decodedData
                final Messenger messenger = new Messenger(new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        decodedData = decodedData.obtain(msg);
                        sendBroadcastToSetUp();
                        sendBroadcastMeasurements();

                    }
                });

                started = false;
                offCounter = 0;
                lastCounter = 0;

                // while the EEG is connected, receive decoded packets from driver (128 per second)
                new Thread(new Runnable() {
                    public void run() {

                        while(started == false && device_turnedOn && device_connected && emotivDevice != null) {
                            try{
                                if(emotivDevice.getData()!=null) {
                                    startActivity(setup_intent);
                                    started = true;
                                } else {
                                    offCounter++;
                                    if(offCounter >= 100) {
                                        device_turnedOn = false;
                                        sendBroadcastToAll(true);
                                        stopSelf();
                                    }
                                }
                                Thread.sleep(6);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        while(device_connected && emotivDevice != null && device_turnedOn) {
                            try {
                                // save decoded packet
                                Message packet = new Message();
                                if(emotivDevice.getData()!=null) {
                                    packet = packet.obtain(emotivDevice.getData());
                                    //System.out.println("counter: "+packet.getData().getInt("counter"));
                                    // forward packet to Messenger
                                    try {
                                        if(last != packet.getData().getInt("counter")) {
                                            messenger.send(packet);
                                            last = packet.getData().getInt("counter");
                                            lastCounter = 0;
                                           // System.out.println(last);
                                        } else {
                                            lastCounter++;
                                            if(lastCounter >=150) {
                                                if(started) {
                                                    device_turnedOn = false;
                                                    sendBroadcastToAll(true);
                                                    stopSelf();
                                                }
                                            }
                                        }
                                    } catch (RemoteException e) {
                                        e.printStackTrace();
                                    }
                                }
                                // sleep for 7,125ms (so 128 packets/sec are received)
                                Thread.sleep(6);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();
        }
        return 1;
    }

    /**
     * when service is stopped, the BroadcastReceivers have to be unregistered
     */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(detached_receiver);
    }
}
