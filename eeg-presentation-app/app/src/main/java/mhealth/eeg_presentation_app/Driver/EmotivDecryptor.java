package mhealth.eeg_presentation_app.Driver;

import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Ina on 30/11/15.
 */
public class EmotivDecryptor {
    private Cipher cipher = null;

    public EmotivDecryptor(String encoded, int vendorID, int productID) {
        try {
            cipher = Cipher.getInstance("AES/ECB/NoPadding");

            SecretKeySpec keySpec = new SecretKeySpec(getKey(encoded, getKeyGenerationMethod(vendorID, productID)), "AES");
            cipher.init(Cipher.DECRYPT_MODE, keySpec);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            e.printStackTrace();
        }
    }

    private int getKeyGenerationMethod(int vendorID, int productID) {
        if(vendorID == 4660 && productID == 60674) {
            return 0;
        }
        return 0;
    }

    public byte[] decrypt(byte[] in) {
        try {
            if (cipher != null) {
                return cipher.doFinal(in);
            } else {
                return null;
            }
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
            return null;
        }

    }

    private byte[] getKey(String serial, int keyGenerationMethod) {

        byte[] k = new byte[16];
        byte[] serialBytes = serial.getBytes(Charset.forName("UTF-8"));

        // Key generation for epoc+ model
        if (keyGenerationMethod == 0) { //this is supposedly only for dev devices but seems to work for
            // epoc devices as well now
            // k = ['\0'] * 16
            // k[0] = serial[-1]
            k[0] = serialBytes[serialBytes.length - 1];
            // k[1] = serial[-2]
            k[1] = serialBytes[serialBytes.length - 2];
            // k[2] = serial[-2]
            k[2] = serialBytes[serialBytes.length - 2];
            //k[3] = serial[-3]
            k[3] = serialBytes[serialBytes.length - 3];
            //k[4] = serial[-3]
            k[4] = serialBytes[serialBytes.length - 3];
            //k[5] = serial[-3]
            k[5] = serialBytes[serialBytes.length - 3];
            //k[6] = serial[-2]
            k[6] = serialBytes[serialBytes.length - 2];
            //k[7] = serial[-4]
            k[7] = serialBytes[serialBytes.length - 4];
            //k[8] = serial[-1]
            k[8] = serialBytes[serialBytes.length - 1];
            //k[9] = serial[-4]
            k[9] = serialBytes[serialBytes.length - 4];
            //k[10] = serial[-2]
            k[10] = serialBytes[serialBytes.length - 2];
            //k[11] = serial[-2]
            k[11] = serialBytes[serialBytes.length - 2];
            //k[12] = serial[-4]
            k[12] = serialBytes[serialBytes.length - 4];
            //k[13] = serial[-4]
            k[13] = serialBytes[serialBytes.length - 4];
            //k[14] = serial[-2]
            k[14] = serialBytes[serialBytes.length - 2];
            //k[15] = serial[-1]
            k[15] = serialBytes[serialBytes.length - 1];

            // ret = ?
            // ['7', '\x00', '4', 'T', '5', '\x10', '0', 'B', '7', '\x00', '4', 'H', '5', '\x00', '0', 'P']
            // Log.i("CRYPTO", "ret = " + new String(k));
            return k;

        }

        return null;

    }
}
