package mhealth.eeg_presentation_app.Driver;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbRequest;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Pair;

import java.nio.ByteBuffer;
import java.util.HashMap;

import mhealth.eeg_presentation_app.Driver.EmotivDecryptor;
import mhealth.eeg_presentation_app.Driver.EmotivElectrodes;

/**
 * Created by Ina on 30/11/15.
 */
public class EmotivReaderRunnable implements Runnable {

    final private UsbDevice device;
    final private UsbManager usbManager;
    private final Messenger messenger;
    final private EmotivDecryptor decryptor;
    final private String serialNumber;
    final private String deviceModel;


    HashMap<EmotivElectrodes, Integer> quality = new HashMap<>();
    /**
     * in full percent
     */
    int battery = 0;
    /**
     * in hours
     */
    double batteryTimeLeft = 0;

    long lastUpdated = 0;

    public EmotivReaderRunnable(UsbDevice device, UsbManager usbManager, EmotivDecryptor decryptor, Messenger messenger, String serialNumber, Context context) {
        this.device = device;
        this.usbManager = usbManager;
        this.messenger = messenger;
        this.decryptor = decryptor;
        this.serialNumber = serialNumber;
        deviceModel = "Emotiv EEG";
    }


    @Override
        public void run() {

        /**
         * set up connection to EEG
         */
        final UsbInterface myInterface = device.getInterface(1); // communicate with interface 1
        final UsbEndpoint myEndpoint = myInterface.getEndpoint(0); // endpoint for communication
        UsbDeviceConnection connection = usbManager.openDevice(device); // start connection
        connection.claimInterface(myInterface, true);
        /**
         * buffer for packet as it arrives, split up in bytes (encrypted)
         */
        ByteBuffer encryptedPacket = ByteBuffer.wrap(new byte[32]);

        UsbRequest usbRequest = new UsbRequest();
        UsbRequest usbResult = null;
        // initialize connection
        usbRequest.initialize(connection, myEndpoint);

        //as long as thread active
        while (!Thread.interrupted()) {
            // read 32 byte data into encryptedPacket, return true if successful
            boolean success = usbRequest.queue(encryptedPacket, 32);
            if (success) {
                // get result of usbRequest
                usbResult = connection.requestWait();
            } else {
                break; //something is wrong with the connection
            }
            // now packet was written to encryptedPacket
            if (encryptedPacket.position() == 0) { //no content
                break;
            }

            // how long since system was booted
            long elapsedNanos = SystemClock.elapsedRealtimeNanos(); //this requires api level 17 (android 4.2)
            //long elapsedNanos = SystemClock.elapsedRealtime(); //temporary, for debugging on my Android 4.1
            // it allows very exact timing

            // standard wall clock time -> time when packet was received
            long receivedTime = System.currentTimeMillis();

            // decrypt packet content with EmotivDecryptor
            byte[] decryptedPacket = decryptor.decrypt(encryptedPacket.array());

            // There are 2 channels: 0x10 for eeg data and 0x20 for ims data
            if (decryptedPacket[1] == 0x10) { // eeg channel
                Message message = Message.obtain();
                message.setData(packetToBundle(elapsedNanos, receivedTime, decryptedPacket));

                try {
                    messenger.send(message);
                } catch (RemoteException e) {
                    break; // the service went away, so let's close this thread as well
                }
            }

        }
            if (usbResult != null) {
                usbResult.close();
            }
            connection.releaseInterface(myInterface);
            connection.close();
        }



    private Bundle packetToBundle(long elapsedNanos, long time, byte[] packet) {

        // first update the state of the connected device
        Pair<EmotivElectrodes, Integer> q = EmotivElectrodes.decodeQuality(packet);
        if (q.first != null) {
            quality.put(q.first, q.second);
        }
        lastUpdated = time;
        if (EmotivElectrodes.containsBattery(packet)) {
            battery = EmotivElectrodes.getBattery(packet);
            batteryTimeLeft = EmotivElectrodes.getBatteryTimeLeft(packet);
        }

        Bundle ret = new Bundle();

        // enter all electrode values from the packet
        for (EmotivElectrodes emotivElectrodes : EmotivElectrodes.values()) {
            ret.putInt(emotivElectrodes.name(), emotivElectrodes.decodeValue(packet));

        }

        for (HashMap.Entry<EmotivElectrodes, Integer> entry : quality.entrySet()) {
            ret.putInt(entry.getKey().name()+"_quality", entry.getValue());
        }


        // put timing info
        ret.putLong("received_elapsed_nanos", elapsedNanos);
        ret.putLong("received_time", time);


        // enter battery and quality from updated state
        ret.putInt("battery", battery);
        ret.putDouble("battery_left", batteryTimeLeft);
        for (EmotivElectrodes emotivElectrodes : quality.keySet()) {
            ret.putInt(emotivElectrodes.name() + "_q", quality.get(emotivElectrodes));
        }

        // enter gyros
        ret.putInt("gyroX", EmotivElectrodes.getGyroX(packet));
        ret.putInt("gyroY", EmotivElectrodes.getGyroY(packet));

        //enter counter
        ret.putInt("counter", EmotivElectrodes.getCounter(packet));

        ret.putBoolean("isSync", EmotivElectrodes.isSync(packet));

        ret.putString("serial", serialNumber);
        ret.putString("model", deviceModel);

        ret.putByteArray("decrypted", packet);
        return ret;
    }


}
