package mhealth.eeg_presentation_app.Driver;

/**
 * Created by Ina on 01/12/15.
 */
public enum EmotivPacketDecoder {


    COUNTER(0, 7),
    AF3_DATA(16, 31),
    F7_DATA(32, 47),
    F3_DATA(48, 63),
    FC5_DATA(64, 79),
    T7_DATA(80, 95),
    P7_DATA(96, 111),
    O1_DATA(112, 127),
    SQ_WAVE(128, 143),
    O2_DATA(144, 159),
    P8_DATA(160, 175),
    T8_DATA(176, 191),
    FC6_DATA(192, 207),
    F4_DATA(208, 223),
    F8_DATA(224, 239),
    AF4_DATA(240, 255);


    private final int firstByteIndex;
    private final int firstByteMask;
    private final int lastByteIndex;
    private final int lastByteMask;
    private final int lastByteNumberOfBits;


    private EmotivPacketDecoder(int from, int toInclusive) {
        firstByteIndex = from / 8;
        firstByteMask = 0xff >> (from % 8);
        lastByteIndex = toInclusive / 8;
        lastByteNumberOfBits = toInclusive % 8 + 1;
        lastByteMask = 0xff >> (8 - lastByteNumberOfBits);
    }


    public int extractFrom(final byte[] decodedPacket) {
        //only works for stuff that fits into ints (4 bytes) right now.

        int ret = 0;

        for (int i = firstByteIndex; i <= lastByteIndex; i++) {
            if (i == firstByteIndex) {
                ret |= decodedPacket[i] & firstByteMask;
            } else { //i = lastByteIndex
                int buf = (decodedPacket[i] & lastByteMask) << 8;
                ret |= buf;
                ret &= 0x0000FFFF; // restrict to positive values
                /* Received data is interpreted as unsigned 2-byte integers. These can exceed a size
                of 32767. The EDF-format, however, uses signed 2-byte integers. Thus, values above
                32767 are converted to negative integers.
                Current fix: subtract (32768) from the received signal. */
                int t = firstByteIndex * 8;
                if ((t > 15 && t < 128) || (t > 143)) {
                    ret = ret - 32768;
                }
            }
        }
        return ret;
    }


}