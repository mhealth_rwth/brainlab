package mhealth.eeg_presentation_app.Driver;

/**
 * Created by Ina on 01/12/15.
 */

import android.hardware.SensorManager;
import android.util.Pair;

public enum EmotivElectrodes {
    F3(0.35f, 0.35f, EmotivPacketDecoder.F3_DATA),
    FC5(0.25f, 0.45f, EmotivPacketDecoder.FC5_DATA),
    AF3(0.4f, 0.2f, EmotivPacketDecoder.AF3_DATA),
    F7(0.2f, 0.3f, EmotivPacketDecoder.F7_DATA),
    T7(0.15f, 0.5f, EmotivPacketDecoder.T7_DATA),
    P7(0.2f, 0.7f, EmotivPacketDecoder.P7_DATA),
    O1(0.4f, 0.8f, EmotivPacketDecoder.O1_DATA),
    O2(0.6f, 0.8f, EmotivPacketDecoder.O2_DATA),
    P8(0.8f, 0.7f, EmotivPacketDecoder.P8_DATA),
    T8(0.85f, 0.5f, EmotivPacketDecoder.T8_DATA),
    F8(0.8f, 0.3f, EmotivPacketDecoder.F8_DATA),
    AF4(0.6f, 0.2f, EmotivPacketDecoder.AF4_DATA),
    FC6(0.75f, 0.45f, EmotivPacketDecoder.FC6_DATA),
    F4(0.65f, 0.35f, EmotivPacketDecoder.F4_DATA);

    private float x;
    private float y;
    private EmotivPacketDecoder decoder;

    EmotivElectrodes(float x, float y, EmotivPacketDecoder decoder) {
        this.x = x;
        this.y = y;
        this.decoder = decoder;
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }


    public int decodeValue(byte[] packet) {
        return decoder.extractFrom(packet);
    }

    public static Pair<EmotivElectrodes, Integer> decodeQuality(byte[] packet) {
        int sensor = EmotivPacketDecoder.COUNTER.extractFrom(packet);
        EmotivElectrodes retLoc = null;
        switch (sensor) {
            case 0:
                retLoc = F3;
                break;
            case 1:
                retLoc = FC5;
                break;
            case 2:
                retLoc = AF3;
                break;
            case 3:
                retLoc = F7;
                break;
            case 4:
                retLoc = T7;
                break;
            case 5:
                retLoc = P7;
                break;
            case 6:
                retLoc = O1;
                break;
            case 7:
                retLoc = O2;
                break;
            case 8:
                retLoc = P8;
                break;
            case 9:
                retLoc = T8;
                break;
            case 10:
                retLoc = F8;
                break;
            case 11:
                retLoc = AF4;
                break;
            case 12:
                retLoc = FC6;
                break;
            case 13:
                retLoc = F4;
                break;


            case 64:
                retLoc = F3;
                break;
            case 65:
                retLoc = FC5;
                break;
            case 66:
                retLoc = AF3;
                break;
            case 67:
                retLoc = F7;
                break;
            case 68:
                retLoc = T7;
                break;
            case 69:
                retLoc = P7;
                break;
            case 70:
                retLoc = O1;
                break;
            case 71:
                retLoc = O2;
                break;
            case 72:
                retLoc = P8;
                break;
            case 73:
                retLoc = T8;
                break;
            case 74:
                retLoc = F8;
                break;
            case 75:
                retLoc = AF4;
                break;
            case 76:
                retLoc = FC6;
                break;
            case 77:
                retLoc = F4;
                break;

            default:
                break;
        }

//        double current_contact_quality = (EmotivPacketDecoder.SQ_WAVE.extractFrom(packet) / 540.) - 1;

        int contactQuality = EmotivPacketDecoder.SQ_WAVE.extractFrom(packet);
        return new Pair<>(retLoc,contactQuality);
    }


    public static boolean containsBattery(byte[] packet) {
        return EmotivPacketDecoder.COUNTER.extractFrom(packet) == 127;
    }

    public static int getBattery(byte[] packet) {
        if (!containsBattery(packet)) {
            return -1;
        }

        int value = EmotivPacketDecoder.SQ_WAVE.extractFrom(packet);
        switch (value) {
            case 57:
                return 100;
            case 56:
                return 99;
            case 55:
                return 98;
            case 54:
                return 96;
            case 53:
                return 92;
            case 52:
                return 81;
            case 51:
                return 60;
            case 50:
                return 40;
            case 49:
                return 28;
            case 48:
                return 18;
            case 47:
                return 9;
            case 46:
                return 3;
            default:
                if (value > 57) {
                    return 100;
                } else if (value < 46) {
                    return 1;
                }
        }
        return -1;


    }

    public static double getBatteryTimeLeft(byte[] packet) {
        if (!containsBattery(packet)) {
            return -1;
        }

        int value = EmotivPacketDecoder.SQ_WAVE.extractFrom(packet);
        switch (value) {
            case 57:
                return 9.5;
            case 56:
                return 9.5;
            case 55:
                return 9.5;
            case 54:
                return 9;
            case 53:
                return 9;
            case 52:
                return 8;
            case 51:
                return 6;
            case 50:
                return 4;
            case 49:
                return 2.5;
            case 48:
                return 1.5;
            case 47:
                return 1;
            case 46:
                return 0.5;
            default:
                if (value > 57) {
                    return 10;
                } else if (value < 46) {
                    return 0;
                }
        }
        return -1;

    }

    public static boolean isSync(byte[] packet) {
        return EmotivPacketDecoder.COUNTER.extractFrom(packet) == 0x39;
    }

    // TODO include ims channel to get gyro data
    public static int getGyroX(byte[] packet) {
        return 1; // EmotivPacketDecoder.GYRO_X.extractFrom(packet) - 103;
    }

    public static int getGyroY(byte[] packet) {
        return 1; // EmotivPacketDecoder.GYRO_Y.extractFrom(packet) - 104;
    }

    public static int getCounter(byte[] packet) {
        return EmotivPacketDecoder.COUNTER.extractFrom(packet);
    }
}