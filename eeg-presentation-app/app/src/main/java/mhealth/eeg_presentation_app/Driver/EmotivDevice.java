package mhealth.eeg_presentation_app.Driver;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.support.annotation.Nullable;

/**
 * Class encapsulating an Emotiv Device
 */
public class EmotivDevice {
    public static final int VENDOR_ID = 4660;
    public static final int PRODUCT_ID = 60674;

    final protected UsbDevice device;
    final protected UsbManager usbManager;
    final protected Context context;

    final private String serial;
    final protected EmotivDecryptor decryptor;
    private Thread readingThread = null;

    private Message message;



    final private Messenger messengerForDriver = new Messenger(new Handler() {
        @Override
        public void handleMessage(Message msg) {

            message = message.obtain(msg);

        }
    });

    public Message getData() {
        return this.message;
    }


    /**
     * Creates object of type EmotivDevice and sets basic attributes
     *
     * @param usbDevice the usb device that is attached and was recognized as Emotiv Device
     *
     */
    public EmotivDevice(UsbDevice usbDevice, UsbManager usbManager, Context context_) {
        this.device = usbDevice;
        this.usbManager = usbManager;
        context = context_;

        serial = extractSerial();
        decryptor = new EmotivDecryptor(serial, device.getVendorId(), device.getProductId());
        this.startReceivingData(messengerForDriver);

    }

    private String getConnectionSerial(UsbDeviceConnection connection) {

        return connection.getSerial();
    }

    /**
     * find out serial number of device
     * @return serial number
     */
    private String extractSerial() {

        UsbDeviceConnection connection = usbManager.openDevice(device);

        String serial;

        // the easy way to get the serial
        serial = getConnectionSerial(connection);
        if (serial != null) {
            System.out.println("Serial found");
            return serial;
        }
        // if the easy way does not work, try interface0
        final UsbInterface interface0 = device.getInterface(0);// reading on interface 0
        connection.claimInterface(interface0, true);
        serial = getConnectionSerial(connection);
        connection.releaseInterface(interface0);
        connection.close();
        if (serial != null) {
            System.out.println("Serial found in Interface0");
            return serial;
        }
        // no serial number found
        return null;
    }

    public boolean startReceivingData(final Messenger messenger) {
        if (readingThread != null) { // Thread already used
            return false;
        }

        // new Thread for reading
        readingThread = new Thread(
                new EmotivReaderRunnable(device, usbManager, decryptor, messenger, serial, context),
                "Emotiv USB dongle reading thread");
        readingThread.start();
        return true; // reading process successfully started
    }




}


