package mhealth.eeg_presentation_app;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLUtils;

import java.nio.FloatBuffer;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by Ina on 22/03/16.
 */
public class MyGLRendererLines extends MyGLRenderer {

    List<FloatBuffer> measurements;
    Boolean rotated = false;


    public MyGLRendererLines(Context context) {
        super(context);
    }

    public void setMeasurements(List<FloatBuffer> measurements) {
        this.measurements = measurements;
    }

    public void setRotated(boolean rotated) {
        this.rotated = rotated;
    }

    public void onDrawFrame(GL10 gl) {
        gl.glClear(GLES20.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        synchronized (measurements) {
            for (int i = 0; i < measurements.size(); i++) {
                gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
                gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
                // 2 Floats = 1 Line
                gl.glVertexPointer(2, GL10.GL_FLOAT, 0, measurements.get(i));
                gl.glDrawArrays(GL10.GL_LINES, 0, measurements.get(i).capacity() / 2);
                gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
            }
        }


        // Redraw background color
//        gl.glClear(GLES20.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity();
        // changing z value = zoom
        if(rotated) {
            gl.glTranslatef(0.0f, 0.1f, -2.3f);
        } else {
            gl.glTranslatef(0.0f, 0.6f, -3.8f);
        }

    }
}
