package mhealth.eeg_presentation_app.Recorder;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Toast;

import org.apache.commons.lang.StringUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;

import mhealth.eeg_presentation_app.DemoViewer.Annotation;
import mhealth.eeg_presentation_app.DemoViewer.Measurement;

public class RecordService extends Service {
    private Handler handler;
    private boolean expFinished = false;
    private static final String TAG = "RecordService";

    private static final String ACTION_STRING_ACTIVITY = "ToAll";
    private static final String ACTION_STRING_MEASURING = "ToMeasuringUsers";
    private static final String ACTION_STRING_STOPRECORDING = "ToRecorder";
    private static final String ACTION_STRING_EVENT = "Event";
    private static final String ACTION_SAVING_STOP = "ToPresenter_Saved";

    private static final int SECONDS_PER_RECORD = 5; // Every record block will be 5 seconds long
    private static final int NUMBER_SAMPLES_PER_RECORD = 128 * SECONDS_PER_RECORD;
    private static int numOfDataRecords = 0;
    private static int saveState = 1;
    private static int [] lastMeasurement;
    private static int [] lastQuality;

    private long startTime = 0;
    private long lastSaveTime = 0;
    private long saveTime = 0;
    private long endTime = 0;

    File record_file;

    // Electrode names
    private String[] electrodes = {"F3","FC5","AF3","F7","T7","P7","O1","O2","P8","T8","F8","AF4","FC6","F4"};

    private ArrayList<Measurement[]> recordList1;
    private ArrayList<Measurement[]> recordList2;
    private ArrayList<Annotation> annotationList1;
    private ArrayList<Annotation> annotationList2;

    private String patientInformation = "";
    private String localInformation = "";
    private String electrodeType = "AgAgCl electrodes";

    private String information;
    private Thread savingTimer;
    private Thread dataWritingThread;
    private boolean recStopped = false;


    public RecordService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Connection to EEG in background, receiving of EEG data packets, handles disconnection of EEG
     */
    public int onStartCommand(Intent intent, int flags, final int startId) {
        Log.d("RecordService", "onStartCommand called");
        handler = new Handler();
        super.onStartCommand(intent,flags,startId);
        recordList1 = new ArrayList<Measurement[]>();
        recordList2 = new ArrayList<Measurement[]>();
        annotationList1 = new ArrayList<Annotation>();
        annotationList2 = new ArrayList<Annotation>();
        String expName = "DemoMeasurement";
        if(intent.getExtras() != null) {
            expName = intent.getExtras().getString("name");
            information = intent.getExtras().getString("information");
            if(!information.equals("")) {
                String[] splittedInformation = information.split(";");
                if(splittedInformation.length >= 1)  patientInformation = splittedInformation[0];
                if(splittedInformation.length >= 2) localInformation = splittedInformation[1];
                if(splittedInformation.length ==3) electrodeType = splittedInformation[2];
            }
        }



        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd HH:mm:ss", Locale.US);
        Date date = new Date();
        record_file = new File("storage/emulated/0/Documents/EEG/", ""+formatter.format(date)+" "+expName+" " +patientInformation+".edf");

        lastMeasurement = new int[14];
        for(int i = 0; i < lastMeasurement.length; i++) {
            lastMeasurement[i] = 0;
        }
        lastQuality = new int[14];
        for(int i = 0; i < lastQuality.length; i++) {
            lastQuality[i] = 0;
        }

        // register Receivers

        if(dataReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_MEASURING);
            registerReceiver(dataReceiver, intentFilter);
        }
        if(recordFinished != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_STOPRECORDING);
            registerReceiver(recordFinished, intentFilter);
            Log.d("RecordService", "STOPRECORDING intent receiver registered");
        }
        if(annotationReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_EVENT);
            registerReceiver(annotationReceiver, intentFilter);
        }


        startTime = System.currentTimeMillis();
        saveTime = startTime;
        numOfDataRecords = 0;
        saveState = 1;
        recStopped = false;
        annotationList1.add(new Annotation(startTime, "StartRecord"));

        // Initialize EDF header
        writeHeader();

        // while the EEG is connected, save every 200ms
        savingTimer = new Thread(new Runnable() {
            public void run() {
                while(!recStopped) {
                    try {
                        // Sleep until next record block
                        Thread.sleep(1000*SECONDS_PER_RECORD);
                        scheduleBuffer();
                        lastSaveTime = saveTime;
                        saveTime = System.currentTimeMillis();
                        writeData();
                        numOfDataRecords++;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        savingTimer.start();

        return Service.START_STICKY;
    }

    private void scheduleBuffer() {
        switch (saveState) {
            case 1:
                recordList2.clear();
                annotationList2.clear();
                saveState = 2;
                break;
            case 2:
                recordList1.clear();
                annotationList1.clear();
                saveState = 1;
                break;
        }
    }

    private void addToRecordList(Measurement[] curMes) {
        switch (saveState) {
            case 1:  recordList1.add(curMes);
                break;
            case 2:  recordList2.add(curMes);
                break;
        }
    }

    private void addToAnnotationList(Annotation curAnnot) {
        switch (saveState) {
            case 1:  annotationList1.add(curAnnot);
                break;
            case 2:  annotationList2.add(curAnnot);
                break;
        }
    }

    private int getRecordListSize() {
        int size = 0;
        switch (saveState) {
            case 1:  size = recordList2.size();
                break;
            case 2:  size = recordList1.size();
                break;
        }
        return size;
    }

    private int getAnnotationListSize() {
        int size = 0;
        switch (saveState) {
            case 1:  size = annotationList2.size();
                break;
            case 2:  size = annotationList1.size();
                break;
        }
        return size;
    }

    private Long getRecordTimestamp(int i, int j) {
        Long timestamp = 0L;
        switch (saveState) {
            case 1:  timestamp = recordList2.get(i)[j].getTimestamp();
                break;
            case 2:  timestamp = recordList1.get(i)[j].getTimestamp();
                break;
        }
        return timestamp;
    }

    private Long getAnnotationTimestamp(int j) {
        Long timestamp = 0L;
        switch (saveState) {
            case 1:  timestamp = annotationList2.get(j).getTimestamp();
                break;
            case 2:  timestamp = annotationList1.get(j).getTimestamp();
                break;
        }
        return timestamp;
    }

    private int getRecordQuality(int i, int j) {
        int quality = 0;
        switch (saveState) {
            case 1:  quality = recordList2.get(i)[j].getQuality();
                break;
            case 2:  quality = recordList1.get(i)[j].getQuality();
                break;
        }
        return quality;
    }

    private int getRecordValue(int i, int j) {
        int value = 0;
        switch (saveState) {
            case 1:  value = recordList2.get(i)[j].getValue();
                break;
            case 2:  value = recordList1.get(i)[j].getValue();
                break;
        }
        return value;
    }

    private String getAnnotationEvent(int j) {
        String value = "";
        switch (saveState) {
            case 1:  value = annotationList2.get(j).getEvent();
                break;
            case 2:  value = annotationList1.get(j).getEvent();
                break;
        }
        return value;
    }

    private class ToastRunnable implements Runnable{
        String text;

        public ToastRunnable(String text) {
            this.text = text;
        }

        @Override
        public void run() {
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * BroadcastReceiver to get decoded EEG measurements
     */
    private BroadcastReceiver dataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle intent_extras = intent.getExtras();

            // while the EEG is connected, receive decoded packets from driver (128 per second)
            new Thread(new Runnable() {
                public void run() {
                    Measurement[] curMes = new Measurement[14];
                    int j=0;
                    for(String electrode : electrodes) {
                        curMes[j] = new Measurement(intent_extras.getInt(electrode), intent_extras.getLong("received_time"), intent_extras.getInt(electrode+"_quality"));
                        j++;
                    }
                    // recordList.add(curMes);
                    addToRecordList(curMes);
                }
            }).start();
        }
    };

    /**
     * BroadcastReceiver to get decoded EEG measurements
     */
    private BroadcastReceiver recordFinished = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            unregisterReceiver(dataReceiver);
            unregisterReceiver(annotationReceiver);
            endTime = System.currentTimeMillis();
            expFinished = intent.getExtras().getBoolean("finished");
            Log.d("RecordService", "recordFinished Broadcast");
            // writeRecords();
            recStopped = true;
            completeHeader();


        }
    };

    /**
     * BroadcastReceiver to get connection status
     * If connection fails, try to safe the recording anyway
     */
    private BroadcastReceiver connectionStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle intent_extras = intent.getExtras();

            new Thread(new Runnable() {
                public void run() {
                    Boolean turnedOff = intent_extras.getBoolean("TurnedOff");
                    // writeRecords();
                }
            }).start();
        }
    };

    /**
     * BroadcastReceiver to get event annotations
     */
    private BroadcastReceiver annotationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle intent_extras = intent.getExtras();

            new Thread(new Runnable() {
                public void run() {
                    Annotation curAnnot = new Annotation(intent_extras.getLong("timestamp"), intent_extras.getString("event"));
                    addToAnnotationList(curAnnot);
                }
            }).start();
        }
    };

    /**
     *  Broadcast to Presenter Activity to inform when saving is done
     */
    private void sendBroadcastToPresenter() {
        Intent intent_presenter = new Intent();
        intent_presenter.setAction(ACTION_SAVING_STOP);
        sendBroadcast(intent_presenter);
    }


    public void writeHeader() {
        // File record_file = new File("filename");

        new Thread(new Runnable() {
            public void run() {
                try {
                    FileWriter fwriter = new FileWriter(record_file, true);
                    BufferedWriter bwriter = new BufferedWriter(fwriter);

                    // version of this data format
                    bwriter.append(StringUtils.rightPad("0", 8));
                    // local patient information
                    if (patientInformation.equals("")) {
                        bwriter.append(StringUtils.rightPad("X X X X", 80));
                    } else {
                        bwriter.append(StringUtils.rightPad(patientInformation, 80));
                    }
                    // local recording information
                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
                    String startdate = formatter.format(new Date());
                    if (localInformation.equals("")) {
                        bwriter.append(StringUtils.rightPad("Startdate " + startdate.toUpperCase() + " X X X", 80));
                    } else {
                        bwriter.append(StringUtils.rightPad("Startdate " + startdate.toUpperCase() + " " + localInformation, 80));
                    }
                    // startdate of recording (dd.mm.yy)
                    formatter = new SimpleDateFormat("dd.MM.yy", Locale.US);
                    startdate = formatter.format(new Date());
                    bwriter.append(startdate);
                    // starttime of recording (hh.mm.ss)
                    formatter = new SimpleDateFormat("HH.mm.ss", Locale.US);
                    String starttime = formatter.format(startTime);
                    bwriter.append(starttime);
                    // number of bytes in header record (256+ ns*256)
                    if (annotationList1.size() > 0) {
                        bwriter.append(StringUtils.rightPad(Integer.toString(256 + (30 * 256)), 8));
                    } else {
                        bwriter.append(StringUtils.rightPad(Integer.toString(256 + (29 * 256)), 8));
                    }
                    // reserved
                    bwriter.append(StringUtils.rightPad("EDF+C", 44)); // "EDF+C"
                    // number of data records (-1 if unknown) -> Start with -1
                    bwriter.append(StringUtils.rightPad(Integer.toString(-1), 8));
                    // duration of a data record in seconds
                    // bwriter.append(StringUtils.rightPad("20.0", 8));
                    bwriter.append(StringUtils.rightPad(Integer.toString(NUMBER_SAMPLES_PER_RECORD / 128), 8));
                    // number of signals (ns) in data record
                    if (annotationList1.size() > 0) {
                        bwriter.append(StringUtils.rightPad("30", 4));
                    } else {
                        bwriter.append(StringUtils.rightPad("29", 4));
                    }

                    // ns*16 ascii : ns*label (electrode labels + annotation)
                    for (String str : electrodes) {
                        bwriter.append(StringUtils.rightPad(str, 16));
                    }
                    // ns*16 ascii : ns*label (electrode quality labels + annotation)
                    for (String str : electrodes) {
                        bwriter.append(StringUtils.rightPad(str+"_q", 16));
                    }
                    // missing value channel
                    bwriter.append(StringUtils.rightPad("MARKER", 16));
                    // annotation channels EDF and EDF+
                    if (annotationList1.size() > 0) {
                        bwriter.append(StringUtils.rightPad("MARKER", 16));
                        // bwriter.append(StringUtils.rightPad("EDF Annotations", 16));
                    }

                    // ns * 80 ascii : ns * transducer type (e.g. AgAgCl electrode)
                    for (int i = 0; i < 28; i++) {
                        bwriter.append(StringUtils.rightPad(electrodeType, 80));
                    }
                    bwriter.append(StringUtils.rightPad(electrodeType, 80));
                    if (annotationList1.size() > 0) {
                        bwriter.append(StringUtils.rightPad(electrodeType, 80));
                        // bwriter.append(StringUtils.rightPad("", 80));
                    }
                    // ns * 8 ascii : ns * physical dimension (e.g. uV)
                    for (int i = 0; i < 28; i++) {
                        bwriter.append(StringUtils.rightPad("uV", 8));
                    }
                    bwriter.append(StringUtils.rightPad("uV", 8));
                    if (annotationList1.size() > 0) {
                        bwriter.append(StringUtils.rightPad("uV", 8));
                        // bwriter.append(StringUtils.rightPad("", 8));
                    }
                    // ns * 8 ascii : ns * physical minimum (e.g. -500 or 34)
                    for (int i = 0; i < 28; i++) {
                        bwriter.append(StringUtils.rightPad("-32768", 8));
                    }
                    bwriter.append(StringUtils.rightPad("-32768", 8));
                    if (annotationList1.size() > 0) {
                        bwriter.append(StringUtils.rightPad("-32768", 8));
                        // bwriter.append(StringUtils.rightPad("-1", 8));
                    }

                    // ns * 8 ascii : ns * physical maximum (e.g. 500 or 40)
                    for (int i = 0; i < 28; i++) {
                        bwriter.append(StringUtils.rightPad("32767", 8));
                    }
                    bwriter.append(StringUtils.rightPad("32767", 8));
                    if (annotationList1.size() > 0) {
                        bwriter.append(StringUtils.rightPad("32767", 8));
                        // bwriter.append(StringUtils.rightPad("1", 8));
                    }
                    // ns * 8 ascii : ns * digital minimum (e.g. -2048)
                    for (int i = 0; i < 28; i++) {
                        bwriter.append(StringUtils.rightPad("-32768", 8));
                    }
                    bwriter.append(StringUtils.rightPad("-32768", 8));
                    if (annotationList1.size() > 0) {
                        bwriter.append(StringUtils.rightPad("-32768", 8));
                        // bwriter.append(StringUtils.rightPad("-32768", 8));
                    }
                    // ns * 8 ascii : ns * digital maximum (e.g. 2047)
                    for (int i = 0; i < 28; i++) {
                        bwriter.append(StringUtils.rightPad("32767", 8));
                    }
                    bwriter.append(StringUtils.rightPad("32767", 8));
                    if (annotationList1.size() > 0) {
                        bwriter.append(StringUtils.rightPad("32767", 8));
                        // bwriter.append(StringUtils.rightPad("32767", 8));
                    }
                    // ns * 80 ascii : ns * prefiltering (e.g. HP:0.1Hz LP:75Hz)
                    for (int i = 0; i < 28; i++) {
                        bwriter.append(StringUtils.rightPad("X", 80));
                    }
                    bwriter.append(StringUtils.rightPad("X", 80));
                    if (annotationList1.size() > 0) {
                        bwriter.append(StringUtils.rightPad("X", 80));
                        // bwriter.append(StringUtils.rightPad("", 80));
                    }

                    // nr of samples in each data record
                    for (int i = 0; i < 28; i++) {
                        bwriter.append(StringUtils.rightPad(Integer.toString(NUMBER_SAMPLES_PER_RECORD), 8));
                    }
                    bwriter.append(StringUtils.rightPad(Integer.toString(NUMBER_SAMPLES_PER_RECORD), 8));
                    if (annotationList1.size() > 0) {
                        bwriter.append(StringUtils.rightPad(Integer.toString(NUMBER_SAMPLES_PER_RECORD), 8));
                        // bwriter.append(StringUtils.rightPad(Integer.toString(60 * annotNumber), 8));
                        // bwriter.append(StringUtils.rightPad(Integer.toString(60), 8));
                    }

                    // ns * 32 ascii : ns * reserved
                    for (int i = 0; i < 28; i++) {
                        bwriter.append(StringUtils.rightPad("", 32));
                    }
                    bwriter.append(StringUtils.rightPad("", 32));
                    if (annotationList1.size() > 0) {
                        bwriter.append(StringUtils.rightPad("", 32));
                        // bwriter.append(StringUtils.rightPad("", 32));
                    }

                    bwriter.flush();
                    bwriter.close();

                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }


    public void completeHeader() {
        // File record_file = new File("filename");

        new Thread( new Runnable() {
            public void run() {
                try {
                    if (expFinished) {
                        handler.post(new ToastRunnable("Exporting. Plase wait..."));
                    }
                    savingTimer.join();
                    dataWritingThread.join();

                    RandomAccessFile fwriter = new RandomAccessFile(record_file, "rw");

                    // version of this data format
                    fwriter.skipBytes(8);
                    // local patient information
                    fwriter.skipBytes(80);
                    // local recording information
                    fwriter.skipBytes(80);
                    // startdate of recording (dd.mm.yy)
                    fwriter.skipBytes(8);
                    // starttime of recording (hh.mm.ss)
                    fwriter.skipBytes(8);
                    // number of bytes in header record (256+ ns*256)
                    fwriter.skipBytes(8);
                    // reserved
                    fwriter.skipBytes(44);

                    // number of data records (-1 if unknown)
                    fwriter.write(StringUtils.rightPad(Integer.toString(numOfDataRecords), 8).getBytes());
                    // duration of a data record in seconds ...

                    fwriter.close();

                    // notification for user if export finished
                    sendBroadcastToPresenter();
                    if (expFinished) {
                        handler.post(new ToastRunnable("Export finished!"));
                    }
                    stopSelf();

                } catch(IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }


    public void writeData() {
        // File record_file = new File("filename");

        dataWritingThread = new Thread(new Runnable() {
            public void run() {
                try {
                    // FileWriter fwriter = new FileWriter(record_file, true);

                    // Start of binary data!!!!

                    // binary data
                    FileOutputStream fos = new FileOutputStream(record_file, true);

                    // int time_rounded = Math.round((endTime - startTime) / 1000f + 0.49f);

                    // channel for storing where values were missing
                    int[] missing = new int[NUMBER_SAMPLES_PER_RECORD];
                    for(int l=0;l<missing.length;l++) {
                        missing[l] =0;
                    }
                    // write electrode measurements for each channel after another
                    for(int j=0; j<14; j++) {
                         // first store all measurements in array - 128 fields per record seconds
                        int[] measurements = new int[NUMBER_SAMPLES_PER_RECORD];
                        // fill array with 0
                        for(int i=0;i<measurements.length;i++) {
                            measurements[i] = 0;
                        }
                        // for each measurement, find out place in array based on their timestamps
                        for(int i=0; i<getRecordListSize();i++) {
                            int curround = Math.round(((getRecordTimestamp(i, j) - lastSaveTime) / 1000f) * 128f);
                            // only write measurements during recording time
                            if (curround >= 0) {
                                if (curround < measurements.length) {
                                    measurements[curround] = getRecordValue(i, j);
                                }
                            }
                        }
                        // If the first measurement of a record is missing, set it to the last measurement of the last record
                        if (measurements[0] == 0) {
                            measurements[0] = lastMeasurement[j];
                        }
                        // write first measurement to file
                        fos.write((byte) measurements[0]);
                        fos.write((byte) (measurements[0] >>> 8));
                        // write all other measurements. if one contains 0 (missing measurement at this place, take value from precessor
                        for (int k = 1; k < measurements.length; k++) {
                            if (measurements[k] == 0) {
                                measurements[k] = measurements[k - 1];
                                // save position of missing value
                                missing[k] = (missing[k] | (int)(Math.pow(2,j)));
                            }
                            fos.write((byte) measurements[k]);
                            fos.write((byte) (measurements[k] >>> 8));
                        }
                        // save the last measurement of a record
                        lastMeasurement[j] = measurements[measurements.length-1];
                    }

                    // write electrode measurements for each channel after another
                    for(int j=0; j<14; j++) {
                        // first store all measurements in array - 128 fields per record seconds
                        int[] measurements = new int[NUMBER_SAMPLES_PER_RECORD];
                        // fill array with -1
                        for(int i=0;i<measurements.length;i++) {
                            measurements[i] = -1;
                        }
                        // for each measurement, find out place in array based on their timestamps
                        for(int i=0; i<getRecordListSize();i++) {
                            int curround = Math.round(((getRecordTimestamp(i, j) - lastSaveTime) / 1000f) * 128f);
                            // only write measurements during recording time
                            if (curround >= 0) {
                                if (curround < measurements.length) {
                                    measurements[curround] = getRecordQuality(i, j);
                                }
                            }
                        }
                        // If the first measurement of a record is missing, set it to the last measurement of the last record
                        if (measurements[0] == -1) {
                            measurements[0] = lastQuality[j];
                        }
                        // write first measurement to file
                        fos.write((byte) measurements[0]);
                        fos.write((byte) (measurements[0] >>> 8));
                        // write all other measurements. if one contains -1 (missing measurement at this place, take value from precessor
                        for (int k = 1; k < measurements.length; k++) {
                            if (measurements[k] == -1) {
                                measurements[k] = measurements[k - 1];
                                // save position of missing value
                                missing[k] = (missing[k] | (int)(Math.pow(2,j)));
                            }
                            fos.write((byte) measurements[k]);
                            fos.write((byte) (measurements[k] >>> 8));
                        }
                        // save the last measurement of a record
                        lastQuality[j] = measurements[measurements.length-1];
                        // if measurements for last electrodes were written, write missing value channel
                        if(j==13) {
                            for(int n=0; n<missing.length; n++) {
                                fos.write((byte) missing[n]);
                                fos.write((byte) (missing[n]>>>8));
                            }
                        }
                    }

                    // write events with timestamps if existing (for EDF with MARKER channel)
                    // first store all events in array - 128 fields per record seconds
                    int[] events = new int[NUMBER_SAMPLES_PER_RECORD];
                    // fill array with 0
                    for(int i=0; i<events.length; i++) {
                        events[i] = 0;
                    }
                    // insert events at correct position based on their timestamp
                    for(int j=0; j<getAnnotationListSize(); j++) {
                        int curround = Math.round(((getAnnotationTimestamp(j) - lastSaveTime)/1000f)*128f);
                        if(curround < events.length) {
                            if(curround < 0) {
                                // Log.d("RecordService","curround is " + curround + ", set it to 0");
                                curround = 0;
                            }
                            //System.out.println(annotationList.get(j).getEvent());
                            int label;
                            // set label only if integer is given or if start- or stop of recording (0). otherwise ignore event
                            try{
                                label = Integer.parseInt(getAnnotationEvent(j));
                                events[curround] = label;
                            }catch(NumberFormatException e){
                                if(getAnnotationEvent(j).equals("StartRecord") || getAnnotationEvent(j).equals("StopRecord")) {
                                    events[curround] = 1000;
                                } else if(getAnnotationEvent(j).equals("Touch")) {
                                    events[curround] = 1001;
                                }
                            }
                        }
                    }
                    // write to file
                    for(int n=0; n<events.length; n++) {
                        fos.write((byte) events[n]);
                        fos.write((byte) (events[n]>>>8));
                    }

                    fos.close();

                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        });
        dataWritingThread.start();

    }


    /**
     * when service is stopped, the BroadcastReceivers have to be unregistered
     */
    public void onDestroy() {
        Log.d("RecordService", "onDestroy() called");
        try {
            unregisterReceiver(dataReceiver);
            unregisterReceiver(annotationReceiver);
        } catch(IllegalArgumentException e) {
        }
        unregisterReceiver(recordFinished);
        super.onDestroy();
    }

}
