package mhealth.eeg_presentation_app;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

import mhealth.eeg_presentation_app.MyGLRenderer;

/**
 * Created by Ina on 21/03/16.
 */
public class MyGLSurfaceView extends MyGLSurfaceViewBasic {


    public MyGLSurfaceView(Context context){

        super(context,new MyGLRenderer(context));

    }

    public MyGLSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs,new MyGLRenderer(context));
        // Create an OpenGL ES 1.0 context
    }

}