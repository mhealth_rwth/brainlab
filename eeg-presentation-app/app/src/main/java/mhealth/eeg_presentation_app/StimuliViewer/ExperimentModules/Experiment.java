package mhealth.eeg_presentation_app.StimuliViewer.ExperimentModules;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import mhealth.eeg_presentation_app.StimuliViewer.StimuliModules.Stimulus;

/**
 * Created by Ina on 27/01/16.
 */
public class Experiment {
    public String expName;
    protected Sequence[] sequences;
    private int sequenceAmount;
    private int sequenceAmountRepeated;
    private int sequentialCounter;
    private boolean random;
    private boolean ordered;
    // create new experiment with certain name and amount of different sequences
    public Experiment(String name, int seq_amount) {
        this.expName = name;
        this.sequences = new Sequence[seq_amount];
        this.sequenceAmount = seq_amount;
        this.ordered = false;
    }

    private ArrayList<Integer[][]> randomizedStimuli = new ArrayList<Integer[][]>();
    private ArrayList<Integer> randomSequences = new ArrayList<Integer>();

    /**
     * if random order of sequences required, generate array with all sequences and their repetitions
     */
    public void prepareSequences() {
        if (random) {
            for (int i = 0; i < sequenceAmount; i++) { // for each sequence
                randomSequences.add(i); // add sequence
                for (int j = 0; j < sequences[i].repeat_times; j++) { // add possible repetitions
                    randomSequences.add(i);
                }
            }
        }
    }

    /**
     * choose sequence to be presented randomly from the array
     * @return number of to be presented sequences
     */
    public int chooseSequence() {
        Integer val;
        if(this.random) {
            int random = (int) (Math.random() * randomSequences.size()); // random array cell
            val = randomSequences.get(random); // get sequence number from cell
            randomSequences.remove(random); // remove the sequence
        } else {
            val = randomSequences.get(0); // get sequence number from cell
            randomSequences.remove(0); // remove the sequence
        }
        return val; // return number
    }

    /**
     * check if there are sequences remaining in the sequence array
     * @return true if sequences available
     */
    public boolean sequenceRemaining() {
        return (randomSequences.size()>0);
    }

    /**
     * prepare array with all stimuli of sequence to be shown, so that we can choose randomly stimuli from this array
     * @param seqNum number of the sequence to which the stimuli belong to
     */
    public void prepareStimuli(int seqNum) {
        if(sequences[seqNum].random) { // if random order of stimuli required
            if(sequences[seqNum].sSingleAmount == 0) { // only overall amount given
                for(int i=0; i<this.sequences[seqNum].sAmount; i++) {
                    Integer[][] cur = {{0,(int)(Math.random()*this.sequences[seqNum].sNumber)}};
                    randomizedStimuli.add(cur);
                }
            } else { // take each stimuli x times
                for(int j=0; j<this.sequences[seqNum].sNumber; j++) {
                   // System.out.println("seq: "+seqNum+"j: "+j);
                    Integer[][] cur = {{0,j}};
                    for(int i=0; i<this.sequences[seqNum].sSingleAmount; i++) {
                        randomizedStimuli.add(cur);
                    }
                }
            }
            if(sequences[seqNum] instanceof OddballSeq) {
                if (((OddballSeq) sequences[seqNum]).dSingleAmount == 0) { // only overall amount given
                    for (int i = 0; i < ((OddballSeq) sequences[seqNum]).dAmount; i++) {
                        Integer[][] cur = {{1, (int) (Math.random() * ((OddballSeq) sequences[seqNum]).dNumber)}};
                        randomizedStimuli.add(cur);
                    }
                } else { // take each stimuli x times
                    for (int j = 0; j < ((OddballSeq) sequences[seqNum]).dNumber; j++) {
                        Integer[][] cur = {{1, j}};
                        for (int i = 0; i < ((OddballSeq) sequences[seqNum]).dSingleAmount; i++) {
                            randomizedStimuli.add(cur);
                        }
                    }
                }
                if (sequences[seqNum] instanceof TargetNontargetSeq) {
                    if (((TargetNontargetSeq) sequences[seqNum]).snNumber > 0) {
                        if (((TargetNontargetSeq) sequences[seqNum]).snSingleAmount == 0) {
                            for (int i = 0; i < ((TargetNontargetSeq) sequences[seqNum]).snAmount; i++) {
                                Integer[][] cur = {{2, (int) (Math.random() * ((TargetNontargetSeq) sequences[seqNum]).snNumber)}};
                                randomizedStimuli.add(cur);
                            }
                        } else { // take each stimuli x times
                            for (int j = 0; j < ((TargetNontargetSeq) sequences[seqNum]).snNumber; j++) {
                                Integer[][] cur = {{2, j}};
                                for (int i = 0; i < ((TargetNontargetSeq) sequences[seqNum]).snSingleAmount; i++) {
                                    randomizedStimuli.add(cur);
                                }
                            }
                        }
                    }
                    if (((TargetNontargetSeq) sequences[seqNum]).dnNumber > 0) {
                        if (((TargetNontargetSeq) sequences[seqNum]).dnSingleAmount == 0) {
                            for (int i = 0; i < ((TargetNontargetSeq) sequences[seqNum]).dnAmount; i++) {
                                Integer[][] cur = {{3, (int) (Math.random() * ((TargetNontargetSeq) sequences[seqNum]).dnNumber)}};
                                randomizedStimuli.add(cur);
                            }
                        } else { // take each stimuli x times
                            for (int j = 0; j < ((TargetNontargetSeq) sequences[seqNum]).dnNumber; j++) {
                                Integer[][] cur = {{3, j}};
                                for (int i = 0; i < ((TargetNontargetSeq) sequences[seqNum]).dnSingleAmount; i++) {
                                    randomizedStimuli.add(cur);
                                }
                            }
                        }
                    }
                }
            }
        }
        this.sequentialCounter = 0;
    }

    /**
     * finish training mode and reset sequentialCounter
     */
    public void finishTraining() {
        this.sequentialCounter = 0;
    }

    /**
     * choose stimulus randomly from the array
     * @param seqNum according sequence
     * @return Integer[][] with [0][0]=0 if standard target stimulus, =1 if standard nontarget, =2 if deviant target and =3 if deviant nontarget
     */
    public Integer[][] chooseStimuli(int seqNum) {
        if(sequences[seqNum].random) { // if random order
            int random = (int) (Math.random() * randomizedStimuli.size());
            Integer[][] val = randomizedStimuli.get(random);
            randomizedStimuli.remove(random);
           // System.out.println("val: " + Integer.toString(val[0][1]));
            return val;
        } else if (sequences[seqNum].ordered) {
            Integer[][] val = {{0,sequences[seqNum].sOrder[sequentialCounter]}};
            sequentialCounter++;
            if(sequentialCounter == this.sequences[seqNum].sAmount) {
                sequentialCounter = 0;
            }
            return val;
        }
        else { // if fixed order, we have only standard target stimuli
            Integer[][] val = {{0,sequentialCounter}};
            sequentialCounter++;
            if(sequentialCounter == this.sequences[seqNum].sNumber) {
                sequentialCounter = 0;
            }
            return val;
        }
    }

    /**
     * choose stimulus from the training array
     * @param seqNum according sequence
     * @return Integer[][] with [0][0]=0 if standard target stimulus, =1 if standard nontarget, =2 if deviant target and =3 if deviant nontarget
     */
    public Integer[][] chooseStimuliTraining(int seqNum) {
        Integer[][] val = {{0,sequences[seqNum].trainingOrder[sequentialCounter]}};
        sequentialCounter++;
        if(sequentialCounter == this.sequences[seqNum].trainingAmount) {
            sequentialCounter = 0;
        }
        return val;
    }

    /**
     * Depending on type, a normal sequence or oddball sequence is created and inserted into the array
     * @param type of sequence: either standard (= normal sequence) or oddball (= oddball sequence)
     * @param seqNum which sequence in the array is referenced
     */
    public void createSeq(String name, int type, int seqNum) {
        switch (type) {
            case 0: sequences[seqNum] = new Sequence();
                break;
            case 1: sequences[seqNum] = new OddballSeq();
                break;
            case 2: sequences[seqNum] = new TargetNontargetSeq();
                break;
            default: sequences[seqNum] = new Sequence();
                break;
        }
        sequences[seqNum].name = name;
    }

    /**
     *
     * @return how many different sequences do we have
     */
    public int getSequenceAmount() {
        return this.sequenceAmount;
    }

    /**
     *
     * @return how many sequences do we have including the repetitions
     */
    public int getSequenceAmountRepeated() {
        return this.sequenceAmountRepeated;
    }

    /**
     * set the amount of the sequences including the repetititons
     * @param amount
     */
    public void setSequenceAmountRepeated(int amount) {
        this.sequenceAmountRepeated = amount;
    }

    /**
     * set how much time should pass between the presentation of 2 stimuli
     * @param distance time distance between stimuli
     * @param seqNum which sequence in the array is referenced
     */
    public void setSeqDistance(int distance, int seqNum) {
        sequences[seqNum].time_distance = distance;
    }

    /**
     * set if the stimuli should appear in random order
     * @param random true if random, false if order as given
     * @param seqNum which sequence in the array is referenced
     */
    public void setSeqRandom(boolean random, int seqNum) {
        sequences[seqNum].random = random;
    }

    /**
     * set if the stimuli should appear in random order
     * @param order comma separated numbers referring to stimuli, giving the order of their presentation
     * @param seqNum which sequence in the array is referenced
     */
    public void setSeqOrder(String order, int seqNum) {
        sequences[seqNum].ordered = true;
        String[] stringArray = order.split(",");
        int[] intArray = new int[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            String numberAsString = stringArray[i];
            intArray[i] = Integer.parseInt(numberAsString) - 1;
        }
        sequences[seqNum].sAmount = intArray.length;
        sequences[seqNum].sOrder = intArray;
    }

    /**
     * set if the stimuli should appear in random order
     * @param order comma separated numbers referring to sequences, giving the order of their presentation
     */
    public void setExpOrder(String order) {
        this.ordered = true;
        this.random = false;
        if (order.equals("FixationCross")) {  // Works only with sequenceAmount > 1
            List<Integer> seqList = new ArrayList<Integer>();
            for (int i = 1; i < sequenceAmount; i++) {
                seqList.add(i);
            }
            java.util.Collections.shuffle(seqList); // shuffle all sequences except the fixation cross (Sequence 1)
            // Set order of sequences for the experiment
            int newSequenceAmount = (sequenceAmount-1)*2;
            for (int i = 0; i < newSequenceAmount; i++) { // for each sequence
                Integer val;
                if (i % 2 != 0) {
                    // Select random sequence, but not 1st sequence (fixation cross)
                    val = seqList.get(0); // get sequence number from cell
                    seqList.remove(0); // remove the sequence
                    randomSequences.add(val); // add sequence
                } else {
                    // Fixation cross in between each sequence
                    randomSequences.add(0);
                }
            }
            this.sequenceAmount = newSequenceAmount;
        } else {
            String[] stringArray = order.split(",");
            for (int i = 0; i < stringArray.length; i++) {
                String numberAsString = stringArray[i];
                randomSequences.add(Integer.parseInt(numberAsString) - 1);
            }
            this.sequenceAmount = stringArray.length;
        }
    }

    /**
     * set how often the sequence should be repeated
     * @param repeat_times how often will the sequence be repeated
     * @param seqNum which sequence in the array is referenced
     */
    public void setSeqRepeatTimes(int repeat_times, int seqNum) {
        sequences[seqNum].repeat_times = repeat_times;
    }

    /**
     * get how many times the sequence should be repeated
     * @param seqNum which sequence in the array is referenced
     * @return how often repetition of sequence
     */
    public int getSeqRepeatTimes(int seqNum) {
        return sequences[seqNum].repeat_times;
    }

    /**
     * set how many of all stimuli should be standard target stimuli
     * @param amount amount of standard target stimuli
     * @param seqNum which sequence in the array is referenced
     */
    public void setSeqStandardAmount(int amount, int seqNum) {
        sequences[seqNum].sAmount = amount;
    }
    /**
     * set how many times each standard target stimulus should be shown
     * @param amount how often each standard target stimulus has to be shown
     * @param seqNum which sequence in the array is referenced
     */
    public void setSeqStandardSingleAmount(int amount, int seqNum) {
        sequences[seqNum].sSingleAmount = amount;

    }
    /**
     * get how many times each standard target stimulus should be shown
     * @param seqNum which sequence in the array is referenced
     */
    public int getSeqStandardSingleAmount(int seqNum) {
        return sequences[seqNum].sSingleAmount;

    }

    /**
     * set how many of all stimuli should be standard nontarget stimuli
     * @param amount amount of standard nontarget stimuli
     * @param seqNum which sequence in the array is referenced
     */
    public void setSeqStandardNontargetAmount(int amount, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).snAmount=amount;
    }
    /**
     * set how many times each standard nontarget stimulus should be shown
     * @param amount how often each standard nontarget stimulus has to be shown
     * @param seqNum which sequence in the array is referenced
     */
    public void setSeqStandardNontargetSingleAmount(int amount, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).snSingleAmount=amount;
    }

    /**
     *
     * @param seqNum
     * @return how many of the stimuli should be standard nontarget
     */
    public int getSeqStandardNontargetAmount(int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).snAmount;
    }
    /**
     *
     * @param seqNum
     * @return how often each of the standard nontarget stimuli should be shown
     */
    public int getSeqStandardNontargetSingleAmount(int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).snSingleAmount;
    }

    /**
     * set how long each standard stimuli should be shown
     * @param dur duration im milliseconds
     * @param seqNum sequence the stimuli belong to
     */
    public void setSeqStandardDuration(int dur, int seqNum) {
        sequences[seqNum].sDuration = dur;
    }

    /**
     * set how long each standard nontarget stimuli should be shown
     * @param dur duration im milliseconds
     * @param seqNum sequence the stimuli belong to
     */
    public void setSeqStandardNontargetDuration(int dur, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).snDuration = dur;
    }

    /**
     * get how long each standard nontarget stimuli should be shown
     * @param seqNum sequence the stimuli belong to
     */
    public int getSeqStandardNontargetDuration(int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).snDuration;
    }

    /**
     * set how long each deviant nontarget stimuli should be shown
     * @param dur duration im milliseconds
     * @param seqNum sequence the stimuli belong to
     */
    public void setSeqDeviantNontargetDuration(int dur, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).dnDuration = dur;
    }

    /**
     * get how long each standard nontarget stimuli should be shown
     * @param seqNum sequence the stimuli belong to
     */
    public int getSeqDeviantNontargetDuration(int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).dnDuration;
    }



    /**
     * initialize array with standard target stimulus
     * @param amount amount of standard target stimuli
     * @param seqNum sequence the stimuli belong to
     */
    public void initSeqStandard(int amount, int seqNum) {
        sequences[seqNum].initStandardStimuli(amount);
    }

    /**
     * initialize array with standard nontarget stimulus
     * @param amount amount of standard nontarget stimuli
     * @param seqNum sequence the stimuli belong to
     */
    public void initSeqStandardNontarget(int amount, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).initStandardNontargetStimuli(amount);
    }

    /**
     * insert a standard target stimulus into the array
     * @param stimulus Stimulus
     * @param number number of the stimulus to be inserted
     * @param seqNum sequence the stimuli belong to
     */
    public void insertSeqStandardStimuli(Stimulus stimulus, int number, int seqNum) {
        sequences[seqNum].insertStandardStimulus(stimulus, number);
    }

    /**
     * insert a standard nontarget stimulus into the array
     * @param stimulus stimulus as byte[]
     * @param number number of the stimulus to be inserted
     * @param seqNum sequence the stimuli belong to
     */
    public void insertSeqStandardNontargetStimuli(Stimulus stimulus, int number, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).insertStandardNontargetStimulus(stimulus, number);
    }

    /**
     *
     * @param seqNum
     * @return true if sequence is oddball experiment
     */
    public boolean isOddball(int seqNum) {
        return (sequences[seqNum] instanceof OddballSeq);
    }

    /**
     *
     * @param seqNum
     * @return true if sequence is oddball experiment
     */
    public boolean isNontarget(int seqNum) {
        return (sequences[seqNum] instanceof TargetNontargetSeq);
    }

    /**
     * set how many of all stimuli should be deviant target stimuli
     * @param amount amount of deviant target stimuli
     * @param seqNum which sequence in the array is referenced
     */
    public void setSeqDeviantAmount(int amount, int seqNum) {
        ((OddballSeq)sequences[seqNum]).dAmount = amount;

    }

    /**
     * set how many of all stimuli should be deviant nontarget stimuli
     * @param amount amount of deviant nontarget stimuli
     * @param seqNum which sequence in the array is referenced
     */
    public void setSeqDeviantNontargetAmount(int amount, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).dnAmount = amount;
    }

    /**
     * set how many times each deviant target stimulus should be shown
     * @param amount how often each deviant target stimulus has to be shown
     * @param seqNum which sequence in the array is referenced
     */
    public void setSeqDeviantSingleAmount(int amount, int seqNum) {
        ((OddballSeq)sequences[seqNum]).dSingleAmount = amount;
    }

    /**
     * set how many times each deviant nontarget stimulus should be shown
     * @param amount how often each deviant nontarget stimulus has to be shown
     * @param seqNum which sequence in the array is referenced
     */
    public void setSeqDeviantNontargetSingleAmount(int amount, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).dnSingleAmount = amount;
    }

    /**
     * set how long each deviant stimuli should be shown
     * @param dur duration im milliseconds
     * @param seqNum sequence the stimuli belong to
     */
    public void setSeqDeviantDuration(int dur, int seqNum) {
        ((OddballSeq)sequences[seqNum]).dDuration = dur;

    }

    /**
     * initialize array with deviant target stimulus
     * @param amount amount of deviant target stimuli
     * @param seqNum sequence the stimuli belong to
     */
    public void initSeqDeviant(int amount, int seqNum) {
        ((OddballSeq)sequences[seqNum]).initDeviantStimuli(amount);
    }

    /**
     * initialize array with deviant nontarget stimulus
     * @param amount amount of deviant nontarget stimuli
     * @param seqNum sequence the stimuli belong to
     */
    public void initSeqDeviantNontarget(int amount, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).initDeviantNontargetStimuli(amount);
    }

    /**
     * insert a deviant target stimulus into the array
     * @param stimulus stimulus object
     * @param number number of the stimulus to be inserted
     * @param seqNum sequence the stimuli belong to
     */
    public void insertSeqDeviantStimuli(Stimulus stimulus, int number, int seqNum) {
        ((OddballSeq)sequences[seqNum]).insertDeviantStimulus(stimulus, number);
    }

    /**
     * insert a deviant nontarget stimulus into the array
     * @param stimulus stimulus object
     * @param number number of the stimulus to be inserted
     * @param seqNum sequence the stimuli belong to
     */
    public void insertSeqDeviantNontargetStimuli(Stimulus stimulus, int number, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).insertDeviantNontargetStimulus(stimulus, number);
    }

    /**
     * get deviant stimulus
     * @param num number of stimulus
     * @param seqNum according sequence
     * @return source code of stimulus
     */
    public Stimulus getSeqDevStimulus(int num, int seqNum) {
        return ((OddballSeq)sequences[seqNum]).getDeviantStimulus(num);
    }

    /**
     * get standard stimulus
     * @param num number of stimulus
     * @param seqNum according sequence
     * @return source code of stimulus
     */
    public Stimulus getSeqStandStimulus(int num, int seqNum) {
        return sequences[seqNum].getStandardStimulus(num);
    }

    /**
     * get standard nontarget stimulus
     * @param num number of stimulus
     * @param seqNum according sequence
     * @return source code of stimulus
     */
    public Stimulus getSeqStandNontargetStimulus(int num, int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).getStandardNontargetStimulus(num);
    }

    /**
     * get deviant nontarget stimulus
     * @param num number of stimulus
     * @param seqNum according sequence
     * @return source code of stimulus
     */
    public Stimulus getSeqDevNontargetStimulus(int num, int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).getDeviantNontargetStimulus(num);
    }

    /**
     *
     * @param seqNum
     * @return how many stimuli should be deviant target
     */
    public int getSeqDeviantAmount(int seqNum) {
        return ((OddballSeq)sequences[seqNum]).dAmount;
    }

    /**
     *
     * @param seqNum
     * @return how many stimuli should be deviant nontarget
     */
    public int getSeqDeviantNontargetAmount(int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).dnAmount;
    }

    /**
     *
     * @param seqNum
     * @return how often each of the deviant target stimuli should be shown
     */
    public int getSeqDeviantSingleAmount(int seqNum) {
        return ((OddballSeq)sequences[seqNum]).dSingleAmount;
    }

    /**
     *
     * @param seqNum
     * @return how often each of the deviant nontarget stimuli should be shown
     */
    public int getSeqDeviantNontargetSingleAmount(int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).dnSingleAmount;
    }

    /**
     *
     * @param seqNum
     * @return duration of deviant stimuli
     */
    public int getSeqDeviantDur(int seqNum) {
        return ((OddballSeq)sequences[seqNum]).dDuration;
    }


    public int getSeqStandardDur(int seqNum) {
        return sequences[seqNum].sDuration;
    }

    public int getSeqStandardAmount(int seqNum) {
        return sequences[seqNum].sAmount;
    }

    public int getSeqStandardNontargetNumber(int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).snNumber;
    }

    public int getSeqDeviantNontargetNumber(int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).dnNumber;
    }

    public void setSeqStandardNumber(int num, int seqNum) {
        sequences[seqNum].sNumber = num;
    }

    public int getSeqStandardNumber(int seqNum) {
        return sequences[seqNum].sNumber;
    }

    public int getSeqDeviantNumber(int seqNum) {
        return ((OddballSeq)sequences[seqNum]).dNumber;
    }

    public int getSeqDistance(int seqNum) {
        return sequences[seqNum].time_distance;
    }

    public void setSeqStimuliType(int type, int seqNum) {
        sequences[seqNum].type = type;
    }

    public int getSeqStimuliType(int seqNum) {
        return sequences[seqNum].type;
    }

    public void setSeqTouch(boolean touch, int seqNum) {
        sequences[seqNum].touch = touch;
    }

    public boolean getSeqTouch(int seqNum) {
        return sequences[seqNum].touch;
    }

  /*  public void setSeqXpos(int x, int seqNum) {
        sequences[seqNum].x;
    }

    public int getSeqXpos(int seqNum) {
        return sequences[seqNum].getXpos();
    }

    public void setSeqYpos(int y, int seqNum) {
        sequences[seqNum].setYpos(y);
    }

    public int getSeqYpos(int seqNum) {
        return sequences[seqNum].getYpos();
    }*/

    public void setSeqsTextColor(String color, int seqNum) {
        sequences[seqNum].s_textcolor = color;
    }

    public String getSeqsTextColor(int seqNum) {
        return sequences[seqNum].s_textcolor;
    }

    public void setSeqdTextColor(String color, int seqNum) {
        ((OddballSeq)sequences[seqNum]).d_textcolor = color;
    }

    public String getSeqdTextColor(int seqNum) {
        return ((OddballSeq)sequences[seqNum]).d_textcolor;
    }

    public void setSeqsnTextColor(String color, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).sn_textcolor = color;
    }

    public String getSeqsnTextColor(int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).sn_textcolor;
    }

    public void setSeqdnTextColor(String color, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).dn_textcolor = color;
    }

    public String getSeqdnTextColor(int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).dn_textcolor;
    }

    public void setSeqsTextSize(int size, int seqNum) {
        sequences[seqNum].s_textsize = size;
    }

    public int getSeqsTextSize(int seqNum) {
        return sequences[seqNum].s_textsize;
    }

    public void setSeqdTextSize(int size, int seqNum) {
        ((OddballSeq)sequences[seqNum]).d_textsize = size;
    }

    public int getSeqdTextSize(int seqNum) {
        return ((OddballSeq)sequences[seqNum]).d_textsize;
    }

    public void setSeqsnTextSize(int size, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).sn_textsize = size;
    }

    public int getSeqsnTextSize(int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).sn_textsize;
    }

    public void setSeqdnTextSize(int size, int seqNum) {
        ((TargetNontargetSeq)sequences[seqNum]).dn_textsize = size;
    }

    public int getSeqdnTextSize(int seqNum) {
        return ((TargetNontargetSeq)sequences[seqNum]).dn_textsize;
    }


    public boolean getRandom() {
        return this.random;
    }

    public void setRandom(boolean random) {
        this.random = random;
    }

    public boolean getOrdered() {
        return this.ordered;
    }

    public void setInstruction(boolean hasInstruction, String instruction, int seqNum) {
        sequences[seqNum].hasInstruction = hasInstruction;
        sequences[seqNum].instruction = instruction;
    }

    public boolean hasInstruction(int seqNum) {
        return sequences[seqNum].hasInstruction;
    }

    public String getInstruction(int seqNum) {
        return sequences[seqNum].instruction;
    }

    public void setBackground(boolean hasBackground, int seqNum) {
        sequences[seqNum].hasBackground = hasBackground;
    }

    public boolean hasBackground(int seqNum) {
        return sequences[seqNum].hasBackground;
    }

    public void setTraining(boolean hasTraining, String training, String targets, int seqNum) {
        sequences[seqNum].hasTraining = hasTraining;
        if (hasTraining) {
            String[] stringArray = training.split(",");
            int[] intArray = new int[stringArray.length];
            for (int i = 0; i < stringArray.length; i++) {
                String numberAsString = stringArray[i];
                intArray[i] = Integer.parseInt(numberAsString) - 1;
            }
            sequences[seqNum].trainingAmount = intArray.length;
            sequences[seqNum].trainingOrder = intArray;

            stringArray = targets.split(",");
            intArray = new int[stringArray.length];
            for (int i = 0; i < stringArray.length; i++) {
                String numberAsString = stringArray[i];
                intArray[i] = Integer.parseInt(numberAsString) - 1;
            }
            sequences[seqNum].trainingTargets = intArray;
        }
        Log.d("Experimet", "setTraining: " + training);
    }

    public boolean hasTraining(int seqNum) {
        return sequences[seqNum].hasTraining;
    }

    public int getSeqTrainingAmount(int seqNum) {
        return sequences[seqNum].trainingAmount;
    }

    public String getSeqName(int seqNum) {
        return sequences[seqNum].name;
    }

    public int[] getSeqTrainingTargets(int seqNum) {
        return sequences[seqNum].trainingTargets;
    }
}
