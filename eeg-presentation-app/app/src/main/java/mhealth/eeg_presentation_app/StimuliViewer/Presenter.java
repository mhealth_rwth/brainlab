package mhealth.eeg_presentation_app.StimuliViewer;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import mhealth.eeg_presentation_app.MainActivity;
import mhealth.eeg_presentation_app.MyGLSurfaceView;
import mhealth.eeg_presentation_app.R;
import mhealth.eeg_presentation_app.Recorder.RecordService;
import mhealth.eeg_presentation_app.StimuliViewer.ExperimentModules.Experiment;
import mhealth.eeg_presentation_app.StimuliViewer.StimuliModules.DecoratedExperimentView;
import mhealth.eeg_presentation_app.StimuliViewer.StimuliModules.AudioStimulus;
import mhealth.eeg_presentation_app.StimuliViewer.StimuliModules.ImageStimulus;
import mhealth.eeg_presentation_app.StimuliViewer.StimuliModules.SimpleExperimentView;
import mhealth.eeg_presentation_app.StimuliViewer.StimuliModules.TextStimulus;

public class Presenter extends AppCompatActivity {
    File experiment_file = null;

    // ImageStimulus imageStimulus;
    DecoratedExperimentView stimulus;
    SimpleExperimentView expComp;

    // Experiment object
    Experiment exp;

    // variables to help play the right sequence
    private static int seqAmount = 0; // amount of available sequences
    private static int curSeq = 0; // currently played sequence
    private static int curType = -1; // 0 if image, 1 if audio, 2 if text as stimuli
    private static int repeated = 0; // how many times has the current sequence already been repeatedly presented?
    private static int trainingRepeated = 0; // how many times has the current training already been repeatedly presented?

    // String for broadcast receiver for stimuli timer
    private static final String ACTION_STRING_TIMER = "ToPresenter";
    private static final String ACTION_TIMER_STOP = "ToPresenter_Stop";
    private static final String ACTION_STRING_STOPRECORDING = "ToRecorder";
    private static final String ACTION_STRING_ACTIVITY = "ToAll";
    private static final String ACTION_STRING_EVENT = "Event";
    private static final String ACTION_SAVING_STOP = "ToPresenter_Saved";
    private static final String ACTION_TRAINING_STOP = "ToTrainingRecorder_Stop";
    private static final String ACTION_TRAINING_FEEDBACK = "ToPresenter_Feedback";

    private String information = "";

    String text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("Presenter", "onCreate() called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presenter);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if(toAllReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_ACTIVITY);
            registerReceiver(toAllReceiver, intentFilter);
        }

        // register Broadcast receiver for timing of stimuli (notification when stimulus is to be played)
        if(timerTickReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_TIMER);
            registerReceiver(timerTickReceiver, intentFilter);
        }

        // register Broadcast receiver for timing of sequences (notification when one sequence finished)
        if(timerStopReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_TIMER_STOP);
            registerReceiver(timerStopReceiver, intentFilter);
        }

        // register Broadcast receiver ending of sequences (notification when saving of the previous sequence has finished)
        if(toPresenterReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_SAVING_STOP);
            registerReceiver(toPresenterReceiver, intentFilter);
        }

        // register Broadcast receiver ending training phases(notification when training has finished)
        if(finishTraining != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_TRAINING_FEEDBACK);
            registerReceiver(finishTraining, intentFilter);
        }

        showAltert();

    }

    public void showAltert() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Experiment Information");

        /* alert.setMessage("Type in:\n 1. Local Patient information \n" + "Example: MCH-0234567 F 02-MAY-1951 Haagse_Harry \n" +
                "2. Local Recording Information \n" + "Example: Startdate 02-MAR-2002 EMG561 BK/JOP Sony. MNC R Median Nerve. \n"
                + "3. Electrode type. Leave field empty if default information should be used \n" + "Example: AgAgCl electrodes \n\n"
                + "Use ; as separator between 1, 2 and 3. \nFor more information check EDF+ specification"); */

        // Simplify the Message options for now
        alert.setMessage("Type in:\n Identification for the experiment.");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Start Experiment", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                information = input.getText().toString();

                // load and parse new experiment.xml file into executable experiment object
                String filepath = getIntent().getExtras().getString("filepath");
                LoadExperiment load = new LoadExperiment();
                load.execute(filepath);

                // avoid orientation change of the device -> otherwise activity (experiment) starts newly
                int currentOrientation = getApplicationContext().getResources().getConfiguration().orientation;
                if(currentOrientation == Configuration.ORIENTATION_PORTRAIT) {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                } else {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
                ((RelativeLayout) findViewById(R.id.presenter_layout_1)).setBackgroundColor(Color.LTGRAY); //BLACK

            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                goToExpOverview();
            }
        });

        alert.show();
    }

    /**
     * BroadcastReceiver to detect when EEG has been disconnected
     */
    private BroadcastReceiver toAllReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent main_intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(main_intent);
            unregisterReceiver(toAllReceiver);
        }
    };

    /**
     *  Broadcast to RecordService with annotations
     */
    protected void sendAnnotationBroadcast(long timestamp, String event) {
        Intent intent_recorder = new Intent();
        intent_recorder.putExtra("timestamp",timestamp);
        intent_recorder.putExtra("event", event);
        intent_recorder.setAction(ACTION_STRING_EVENT);
        getApplicationContext().sendBroadcast(intent_recorder);
    }

    /**
     *  AsyncTask method to load experiment and conduct it afterwards
     */
    class LoadExperiment extends AsyncTask<String, Void, Boolean> {
        // first load experiment and generate experiment object
        protected Boolean doInBackground(String...params) {
            // get experiment file and parse it
            experiment_file = new File(params[0]);
            ExperimentParser experimentParser = new ExperimentParser(experiment_file, getApplicationContext());
            exp = experimentParser.getExperiment(); // experiment object
            expComp = new SimpleExperimentView((RelativeLayout) findViewById(R.id.presenter_layout_1));
            // if random sequences wanted, prepare randomization of sequences
            if(exp.getRandom()) {
                exp.prepareSequences();
            }

            return true;
        }

        // after experiment has been loaded and prepared, conduct it
        @Override
        protected void onPostExecute(Boolean bool) {
            // set background color
            expComp.showBackground();
            // if random sequences wanted, choose sequence randomly
            if(exp.getRandom()) {
                curSeq = exp.chooseSequence();
            } else if (exp.getOrdered()) {
                curSeq = exp.chooseSequence();
            } else { // otherwise start with first sequence
                curSeq = 0;
            }

            seqAmount = exp.getSequenceAmount();
            curType = exp.getSeqStimuliType(curSeq);
            exp.prepareStimuli(curSeq);
            // start first sequence!
            if (exp.hasInstruction(curSeq)) {
                startSequenceWithInstruction(curSeq);
            } else {
                startSequence(curSeq); // start playing new sequence
            }
        }
    }



    /**
     * Present stimuli from one sequence
     * @param i sequence number
     */ 
    private void startSequence(int i) {
        Intent service_intent = new Intent(getApplicationContext(), RecordService.class);
        service_intent.putExtra("name",this.exp.expName+" "+exp.getSeqName(curSeq));
        service_intent.putExtra("information", this.information);
        startService(service_intent);
        service_intent = new Intent(getApplicationContext(), StimuliTimer.class);
        service_intent.putExtra("training", false); // set training mode to false
        // get duration of stimuli and the time between the presentation of 2 stimuli
        service_intent.putExtra("distance",exp.getSeqDistance(i)); // time between two stimuli
        service_intent.putExtra("duration", exp.getSeqStandardDur(i)); // duration of standard stimuli
        if(exp.isNontarget(i)) { // if oddball experiment, we need standard and deviant stimuli
            if(exp.getSeqStandardAmount(i) == 0) { // No overall amount of stimuli per sequence but we have values of how many times each single stimuli should be shown
                service_intent.putExtra("amount", (exp.getSeqStandardSingleAmount(i)*exp.getSeqStandardNumber(i))
                        +(exp.getSeqDeviantSingleAmount(i)*exp.getSeqDeviantNumber(i))
                        +(exp.getSeqStandardNontargetSingleAmount(i)*exp.getSeqStandardNontargetNumber(i))
                        +(exp.getSeqDeviantNontargetSingleAmount(i)*exp.getSeqDeviantNontargetNumber(i))); // count how many (not unique) stimuli will be shown
            } else { // random distribution of how many times a single stimuli is presented -> only value for how many stimuli should be shown generally in this sequence
                service_intent.putExtra("amount", exp.getSeqStandardAmount(i)+exp.getSeqDeviantAmount(i)+exp.getSeqStandardNontargetAmount(i)+exp.getSeqDeviantNontargetAmount(i));
            }
        } else if(exp.isOddball(i)) {
            if(exp.getSeqStandardAmount(i) == 0) { // No overall amount of stimuli per sequence but we have values of how many times each single stimuli should be shown
                service_intent.putExtra("amount", (exp.getSeqStandardSingleAmount(i)*exp.getSeqStandardNumber(i))
                        +(exp.getSeqDeviantSingleAmount(i)*exp.getSeqDeviantNumber(i))); // count how many (not unique) stimuli will be shown
            } else { // random distribution of how many times a single stimuli is presented -> only value for how many stimuli should be shown generally in this sequence
                service_intent.putExtra("amount", exp.getSeqStandardAmount(i)+exp.getSeqDeviantAmount(i));
            }
        }
        else { // only standard stimuli
            service_intent.putExtra("amount", exp.getSeqStandardAmount(i));
        }

        if(exp.hasBackground(curSeq)) {
            // if fixation cross should be presented in background
            TextView text = ((TextView) findViewById(R.id.presenter_cross));
            text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 128);
            text.setTextColor(Color.BLACK);
            text.setText("+");
        }

        // if touch reaction of user is required, activate touch recognition
        if(exp.getSeqTouch(curSeq)) {
            /*findViewById(R.id.presenter_layout_1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Toast.makeText(getApplicationContext(),"Touched",Toast.LENGTH_SHORT).show(); // when user touches display
                    sendAnnotationBroadcast(System.currentTimeMillis(), "Touch");
                }
            });*/
            Button touchField = (Button) findViewById(R.id.touch_field);
            touchField.setVisibility(View.VISIBLE);

            touchField.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendAnnotationBroadcast(System.currentTimeMillis(), "Touch");
                }
            });
        }
        // start service to time the presentation of the stimuli
        startService(service_intent);
    }


    /**
     * Present instruction for a sequence and then the stimuli
     * @param i sequence number
     */
    private void startSequenceWithInstruction(int i) {
        // getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        TextView text = ((TextView) findViewById(R.id.presenter_information));
        text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
        text.setTextColor(Color.BLACK); // WHITE
        text.setGravity(Gravity.CENTER);

        text.setText(exp.getInstruction(i)); // present the instruction for the sequence first
        findViewById(R.id.presenter_layout_1).setOnClickListener(new View.OnClickListener() { // enable tapping
                @Override
                public void onClick(View v) { // when user taps on screen
                    ((TextView) findViewById(R.id.presenter_information)).setText(""); // clear text
                    findViewById(R.id.presenter_layout_1).setClickable(false); // disable clicking
                    if (exp.hasTraining(curSeq)) {
                        startSequenceTraining(curSeq); // start sequence training mode first
                    } else {
                        startSequence(curSeq); // start playing new sequence
                    }
                }
        });
    }


    /**
     * Present instruction for a sequence and then the stimuli
     * @param i sequence number
     */
    private void startSequenceTraining(int i) {
        Log.w("dPRESENTER", "startSequenceTraining for sequence " + i);
        Intent service_intent = new Intent(getApplicationContext(), TrainingRecorder.class);
        service_intent.putExtra("targetList", exp.getSeqTrainingTargets(i));
        startService(service_intent);

        service_intent = new Intent(getApplicationContext(), StimuliTimer.class);
        service_intent.putExtra("training", true); // set training mode
        // get duration of stimuli and the time between the presentation of 2 stimuli
        service_intent.putExtra("distance",exp.getSeqDistance(i)); // time between two stimuli
        service_intent.putExtra("duration", exp.getSeqStandardDur(i)); // duration of standard stimuli
        service_intent.putExtra("amount", exp.getSeqTrainingAmount(i)); // amount of stimuli shown in training phase

        if(exp.hasBackground(curSeq)) {
            // if fixation cross should be presented in background
            TextView text = ((TextView) findViewById(R.id.presenter_cross));
            text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 128);
            text.setTextColor(Color.BLACK);
            text.setText("+");
        }

        // if touch reaction of user is required, activate touch recognition
        if(exp.getSeqTouch(curSeq)) {
            /*findViewById(R.id.presenter_layout_1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Toast.makeText(getApplicationContext(),"Touched",Toast.LENGTH_SHORT).show(); // when user touches display
                    sendAnnotationBroadcast(System.currentTimeMillis(), "Touch");
                }
            });*/
            Button touchField = (Button) findViewById(R.id.touch_field);
            touchField.setVisibility(View.VISIBLE);

            touchField.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendAnnotationBroadcast(System.currentTimeMillis(), "Touch");
                }
            });
        }

        // start service to time the presentation of the stimuli
        startService(service_intent);
    }


    /**
     * BroadcastReceiver for the timing of the sequences
     */
    private BroadcastReceiver finishTraining = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) { // last training finished
            Log.d("Presenter", "finishTraining received");
            Intent service_intent = new Intent(getApplicationContext(), TrainingRecorder.class);
            stopService(service_intent);
            exp.finishTraining();

            Bundle intent_extras = intent.getExtras();
            int[] feedback = intent_extras.getIntArray("feedback");
            String numTargets = Integer.toString(feedback[0]);
            String numTargetsClicked = Integer.toString(feedback[1]);
            String numNonTargetsClicked = Integer.toString(feedback[2]);

            TextView text = ((TextView) findViewById(R.id.presenter_information));
            //text.setSingleLine(false);
            text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
            text.setTextColor(Color.BLACK); // WHITE
            text.setGravity(Gravity.CENTER);

            text.setText(getString(R.string.training_finished, numTargetsClicked, numTargets, numNonTargetsClicked));

            if (trainingRepeated < 1) {
                Button repeatTraining = (Button) findViewById(R.id.button_repeat_training);
                repeatTraining.setVisibility(View.VISIBLE);

                repeatTraining.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((TextView) findViewById(R.id.presenter_information)).setText(""); // clear text
                        Button repeatTraining = (Button) findViewById(R.id.button_repeat_training);
                        repeatTraining.setVisibility(View.GONE);
                        Button continueSequence = (Button) findViewById(R.id.button_continue_sequence);
                        continueSequence.setVisibility(View.GONE);
                        trainingRepeated++;
                        startSequenceTraining(curSeq);
                    }
                });
            }

            Button continueSequence = (Button) findViewById(R.id.button_continue_sequence);
            continueSequence.setVisibility(View.VISIBLE);

            continueSequence.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((TextView) findViewById(R.id.presenter_information)).setText(""); // clear text
                    Button repeatTraining = (Button) findViewById(R.id.button_repeat_training);
                    repeatTraining.setVisibility(View.GONE);
                    Button continueSequence = (Button) findViewById(R.id.button_continue_sequence);
                    continueSequence.setVisibility(View.GONE);
                    trainingRepeated = 0;
                    startSequence(curSeq);
                }
            });

        }
    };



    /**
     * BroadcastReceiver for the timing of the stimuli
     */
    private BroadcastReceiver timerTickReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle intent_extras = intent.getExtras();
            boolean stimuli = intent_extras.getBoolean("stimuli");
            if(stimuli) { // if true, it is time to play a new stimulus
                boolean training = intent_extras.getBoolean("training");
                if (training) {
                    playStimulusTraining();
                } else {
                    playStimulus();
                }
            } else { // it is time for a pause between 2 stimuli = show nothing
                //((MyGLSurfaceView) findViewById(R.id.GlView)).getRenderer().deactivateStimuli();
                ((TextView) findViewById(R.id.presenter_information)).setTextColor(Color.TRANSPARENT);
            }
        }
    };

    /**
     * BroadcastReceiver for the timing of the sequences
     */
    private BroadcastReceiver timerStopReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) { // last played sequence finished
            findViewById(R.id.presenter_layout_1).setClickable(false);

            Button touchField = (Button) findViewById(R.id.touch_field);
            touchField.setVisibility(View.GONE);

            Bundle intent_extras = intent.getExtras();
            boolean training = intent_extras.getBoolean("training");
            Intent service_intent;
            service_intent = new Intent(getApplicationContext(), StimuliTimer.class);
            stopService(service_intent);
            // clear view
            ((ImageView) findViewById(R.id.stimuli)).setImageResource(android.R.color.transparent);
            // clear text
            ((TextView) findViewById(R.id.presenter_information)).setText("");
            ((TextView) findViewById(R.id.presenter_cross)).setText("");
            // Text
            TextView text = ((TextView) findViewById(R.id.presenter_information));
            text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
            text.setTextColor(Color.BLACK); // WHITE
            text.setGravity(Gravity.CENTER);
            text.setText(getString(R.string.waiting_text));
            if (!training) {
                sendAnnotationBroadcast(System.currentTimeMillis(), "StopRecord");
                if(((exp.getRandom() || exp.getOrdered()) && !exp.sequenceRemaining()) || (!exp.getRandom() && !(curSeq < seqAmount))) {
                    sendBroadcastToRecorder(false); // true
                } else {
                    sendBroadcastToRecorder(false);
                }
            } else if (training) {
                sendBroadcastToTrainingRecorder();
            }
        }
    };


    /**
     * BroadcastReceiver to detect when RecordService finished saving
     */
    private BroadcastReceiver toPresenterReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("dPRESENTER", "Received saving Stop from recorder");
            Intent service_intent = new Intent(getApplicationContext(), RecordService.class);
            stopService(service_intent);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            TextView text = ((TextView) findViewById(R.id.presenter_information));
            text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
            text.setTextColor(Color.BLACK); // WHITE
            text.setGravity(Gravity.CENTER);
            // if randomized sequences are used
            if (exp.getRandom() || exp.getOrdered()) {
                if (exp.sequenceRemaining()) { // if sequences are remaining, choose randomly the next sequence to be presented
                    curSeq = exp.chooseSequence();
                    text.setText(getString(R.string.sequence_finished)); // announce the end of the last sequence to user
                    findViewById(R.id.presenter_layout_1).setOnClickListener(new View.OnClickListener() { // enable tapping
                        @Override
                        public void onClick(View v) { // when user taps on screen
                            findViewById(R.id.presenter_layout_1).setClickable(false); // disable clicking
                            curType = exp.getSeqStimuliType(curSeq); // get stimuli type of the new sequnence
                            exp.prepareStimuli(curSeq);
                            ((TextView) findViewById(R.id.presenter_information)).setText(""); // clear text
                            if (exp.hasInstruction(curSeq)) {
                                startSequenceWithInstruction(curSeq);
                            } else {
                                startSequence(curSeq); // start playing new sequence
                            }
                        }
                    });
                } else { // no sequences remaining
                    // wait for user to tap on screen to return to the menue
                    text.setText(getString(R.string.experiment_finished));
                    findViewById(R.id.presenter_layout_1).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            findViewById(R.id.presenter_layout_1).setClickable(false);
                            goToExpOverview();
                            // unregister receivers!
                            unregisterReceiver(timerStopReceiver);
                            unregisterReceiver(timerTickReceiver);
                            unregisterReceiver(toPresenterReceiver);
                            unregisterReceiver(finishTraining);
                        }
                    });
                }
                // if fixed order of sequences is used and current sequence does not have to be repeated
            } else {
                if (repeated >= exp.getSeqRepeatTimes(curSeq)) {
                    curSeq++; // next sequence
                    repeated = 0;
                }

                if (curSeq < seqAmount) { // if there are sequences remaining
                    // wait for user to tap on screen to continue
                    text.setText(getString(R.string.sequence_finished));
                    findViewById(R.id.presenter_layout_1).setOnClickListener(new View.OnClickListener() { // enable tapping
                        @Override
                        public void onClick(View v) { // when user taps on screen
                            findViewById(R.id.presenter_layout_1).setClickable(false); // disable clicking
                            curType = exp.getSeqStimuliType(curSeq); // get stimuli type of the new sequnence
                            repeated++;
                            exp.prepareStimuli(curSeq);
                            ((TextView) findViewById(R.id.presenter_information)).setText(""); // clear text
                            if (exp.hasInstruction(curSeq)) {
                                startSequenceWithInstruction(curSeq);
                            } else {
                                startSequence(curSeq); // start playing new sequence
                            }
                        }
                    });
                } else { // no sequences remaining
                    // wait for user to tap on screen to return to the menue
                    text.setText(getString(R.string.experiment_finished));
                    findViewById(R.id.presenter_layout_1).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            findViewById(R.id.presenter_layout_1).setClickable(false);
                            goToExpOverview();
                            // unregister receivers!
                            unregisterReceiver(timerStopReceiver);
                            unregisterReceiver(timerTickReceiver);
                            unregisterReceiver(toPresenterReceiver);
                            unregisterReceiver(finishTraining);
                        }
                    });
                }

            }
        }
    };


    /**
     * Method to play a single stimuli
     * Random selection of stimuli (standard or deviant)
     */
    private void playStimulus() {
        curType = exp.getSeqStimuliType(curSeq); // check if image, audio or text stimuli
        if(curType == 0) { // if image stimulus
            Integer[][] selected = exp.chooseStimuli(curSeq);
            //    System.out.println("seq: "+curSeq);
            //    if(exp.sequences[curSeq] instanceof TargetNontargetSeq) System.out.println("Nontarget");
            //    else if(exp.sequences[curSeq] instanceof OddballSeq) System.out.println("Oddball");
            if(selected[0][0] == 0) { // standard target
                ((MyGLSurfaceView) findViewById(R.id.GlView)).getRenderer().setDuration(exp.getSeqStandardDur(curSeq));
                stimulus = new ImageStimulus(getApplicationContext(), expComp, ((MyGLSurfaceView) findViewById(R.id.GlView)).getRenderer(),
                        exp.getSeqStandStimulus(selected[0][1], curSeq));
            } else if(selected[0][0] == 2) { // standard nontarget
                //       System.out.println("nontarget stimuli");
                ((MyGLSurfaceView) findViewById(R.id.GlView)).getRenderer().setDuration(exp.getSeqStandardNontargetDuration(curSeq));
                stimulus = new ImageStimulus(getApplicationContext(), expComp, ((MyGLSurfaceView) findViewById(R.id.GlView)).getRenderer(),
                        exp.getSeqStandNontargetStimulus(selected[0][1], curSeq));
            } else if(selected[0][0] == 1) { // deviant target
                ((MyGLSurfaceView) findViewById(R.id.GlView)).getRenderer().setDuration(exp.getSeqDeviantDur(curSeq));
                stimulus = new ImageStimulus(getApplicationContext(), expComp, ((MyGLSurfaceView) findViewById(R.id.GlView)).getRenderer(),
                        exp.getSeqDevStimulus(selected[0][1], curSeq));
            } else { // deviant nontarget
                ((MyGLSurfaceView) findViewById(R.id.GlView)).getRenderer().setDuration(exp.getSeqStandardNontargetDuration(curSeq));
                stimulus = new ImageStimulus(getApplicationContext(), expComp, ((MyGLSurfaceView) findViewById(R.id.GlView)).getRenderer(),
                        exp.getSeqDevNontargetStimulus(selected[0][1], curSeq));
            }
            ((ImageStimulus)stimulus).displayImage();
        }
        else if(curType == 1) { // if audio stimulus
            Integer[][] selected = exp.chooseStimuli(curSeq);
            if(selected[0][0] == 0) { // standard target
                stimulus = new AudioStimulus(getApplicationContext(), expComp, exp.getSeqStandStimulus(selected[0][1], curSeq));
            } else if(selected[0][0] == 2) { // standard nontarget
                stimulus = new AudioStimulus(getApplicationContext(), expComp, exp.getSeqStandNontargetStimulus(selected[0][1], curSeq));
            } else if(selected[0][0] == 1) { // deviant target
                stimulus = new AudioStimulus(getApplicationContext(), expComp, exp.getSeqDevStimulus(selected[0][1], curSeq));
            } else { // deviant nontarget
                stimulus = new AudioStimulus(getApplicationContext(), expComp, exp.getSeqDevNontargetStimulus(selected[0][1], curSeq));
            }
            ((AudioStimulus)stimulus).playTone();
        }
        else if(curType == 2) { // if text stimuli
            Integer[][] selected = exp.chooseStimuli(curSeq);
            if(selected[0][0] == 0) { // standard target
                stimulus = new TextStimulus(getApplicationContext(), expComp, (TextView) findViewById(R.id.presenter_information),
                        exp.getSeqStandStimulus(selected[0][1], curSeq), exp.getSeqsTextSize(curSeq), exp.getSeqsTextColor(curSeq));
            } else if(selected[0][0] == 2) { // standard nontarget
                stimulus = new TextStimulus(getApplicationContext(), expComp, (TextView) findViewById(R.id.presenter_information),
                        exp.getSeqStandNontargetStimulus(selected[0][1], curSeq), exp.getSeqdTextSize(curSeq), exp.getSeqdTextColor(curSeq));
            } else if(selected[0][0] == 1) {  // deviant target
                stimulus = new TextStimulus(getApplicationContext(), expComp, (TextView) findViewById(R.id.presenter_information),
                        exp.getSeqDevStimulus(selected[0][1], curSeq), exp.getSeqdTextSize(curSeq), exp.getSeqdTextColor(curSeq));
            } else { // deviant nontarget
                stimulus = new TextStimulus(getApplicationContext(), expComp, (TextView) findViewById(R.id.presenter_information),
                        exp.getSeqDevNontargetStimulus(selected[0][1], curSeq), exp.getSeqdnTextSize(curSeq), exp.getSeqdnTextColor(curSeq));
            }
            ((TextStimulus)stimulus).showText();
        }
    }


    /**
     * Method to play a single stimuli
     * Random selection of stimuli (standard or deviant)
     */
    private void playStimulusTraining() {
        curType = exp.getSeqStimuliType(curSeq); // check if image, audio or text stimuli
        if(curType == 0) { // if image stimulus
            Integer[][] selected = exp.chooseStimuliTraining(curSeq);

            ((MyGLSurfaceView) findViewById(R.id.GlView)).getRenderer().setDuration(exp.getSeqStandardDur(curSeq));
            stimulus = new ImageStimulus(getApplicationContext(), expComp, ((MyGLSurfaceView) findViewById(R.id.GlView)).getRenderer(),
                    exp.getSeqStandStimulus(selected[0][1], curSeq));
            ((ImageStimulus)stimulus).displayImage();
        }
        else if(curType == 1) { // if audio stimulus
            Integer[][] selected = exp.chooseStimuliTraining(curSeq);
            stimulus = new AudioStimulus(getApplicationContext(), expComp, exp.getSeqStandStimulus(selected[0][1], curSeq));
            ((AudioStimulus)stimulus).playTone();
        }
        else if(curType == 2) { // if text stimuli
            Integer[][] selected = exp.chooseStimuliTraining(curSeq);
            stimulus = new TextStimulus(getApplicationContext(), expComp, (TextView) findViewById(R.id.presenter_information),
                    exp.getSeqStandStimulus(selected[0][1], curSeq), exp.getSeqsTextSize(curSeq), exp.getSeqsTextColor(curSeq));
            ((TextStimulus)stimulus).showText();
        }
    }


    /**
     * When service is destroyed, unregister all Receivers
     */
    public void onDestroy() {
        Log.d("Presenter", "onDestroy() called");
        super.onDestroy();
        // deregister Receivers
        try {
            unregisterReceiver(timerTickReceiver);
            unregisterReceiver(timerStopReceiver);
            unregisterReceiver(toPresenterReceiver);
            unregisterReceiver(finishTraining);
            unregisterReceiver(toAllReceiver);
        } catch(IllegalArgumentException e) {

        }
    }

    public void onPause() {
        Log.d("Presenter", "onPause() called");
        super.onPause();
        super.onStop();
        Intent service_intent = new Intent(getApplicationContext(), StimuliTimer.class);
        stopService(service_intent);
        sendBroadcastToRecorder(true);
        try {
            service_intent = new Intent(getApplicationContext(), RecordService.class);
            stopService(service_intent);
            unregisterReceiver(timerTickReceiver);
            unregisterReceiver(timerStopReceiver);
            unregisterReceiver(toPresenterReceiver);
            unregisterReceiver(finishTraining);
            unregisterReceiver(toAllReceiver);
        } catch(IllegalArgumentException e) {
            //e.printStackTrace();
        }
    }

    /**
     * When service stops, unregister all Receivers
     */
    public void onStop() {
        Log.d("Presenter", "onStop() called");
        super.onStop();
        Intent service_intent = new Intent(getApplicationContext(), StimuliTimer.class);
        stopService(service_intent);
        sendBroadcastToRecorder(true);
        try {
            service_intent = new Intent(getApplicationContext(), RecordService.class);
            stopService(service_intent);
            unregisterReceiver(timerTickReceiver);
            unregisterReceiver(timerStopReceiver);
            unregisterReceiver(toPresenterReceiver);
            unregisterReceiver(finishTraining);
            unregisterReceiver(toAllReceiver);
        } catch(IllegalArgumentException e) {
            //e.printStackTrace();
        }
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    /**
     *  Broadcast to SetUp Activity with specific data
     */
    private void sendBroadcastToRecorder(boolean finished) {
        Intent intent_recorder = new Intent();
        intent_recorder.putExtra("finished",finished);
        intent_recorder.setAction(ACTION_STRING_STOPRECORDING);
        sendBroadcast(intent_recorder);
    }


    /**
     *  Broadcast to SetUp Activity with specific data
     */
    private void sendBroadcastToTrainingRecorder() {
        Intent intent_recorder = new Intent();
        intent_recorder.setAction(ACTION_TRAINING_STOP);
        sendBroadcast(intent_recorder);
    }

    /**
     * When service is restarted
     */
    public void onRestart() {
        Log.d("Presenter", "onRestart() called");
        Intent service_intent = new Intent(getApplicationContext(), StimuliTimer.class);
        stopService(service_intent);
        super.onRestart();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        stimulus = null;
        expComp = null;
        exp = null;
        seqAmount = 0;
        curSeq = 0;
        curType = -1;
        repeated = 0;

        // load and parse new experiment.xml file into executable experiment object
        String filepath = getIntent().getExtras().getString("filepath");
        LoadExperiment load = new LoadExperiment();
        load.execute(filepath);
        // register Broadcast receiver for timing of stimuli (notification when stimulus is to be played)
        if(timerTickReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_TIMER);
            registerReceiver(timerTickReceiver, intentFilter);
        }

        // register Broadcast receiver for timing of sequences (notification when one sequence finished)
        if(timerStopReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_TIMER_STOP);
            registerReceiver(timerStopReceiver, intentFilter);
        }

        // register Broadcast receiver ending of sequences (notification when saving of the previous sequence has finished)
        if(toPresenterReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_SAVING_STOP);
            registerReceiver(toPresenterReceiver, intentFilter);
        }

        // register Broadcast receiver ending training phases(notification when training has finished)
        if(finishTraining != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_TRAINING_FEEDBACK);
            registerReceiver(finishTraining, intentFilter);
        }
    }

    /**
     * Start ExperimentOverview
     *
     */
    public void goToExpOverview() {
        Log.d("Presenter", "goToExpOverview() called");
        Intent intent = new Intent(this, ExperimentOverview.class);
        startActivity(intent);
    }


    public void onResume() {
        Log.d("Presenter", "onResume() called");
        super.onResume();
    }

/*    public void onResume() {
        Intent service_intent = new Intent(getApplicationContext(), StimuliTimer.class);
        stopService(service_intent);
        super.onResume();
        stimulus = null;
        expComp = null;
        exp = null;
        seqAmount = 0;
        curSeq = 0;
        curType = -1;
        repeated = 0;

        // load and parse new experiment.xml file into executable experiment object
        String filepath = getIntent().getExtras().getString("filepath");
        LoadExperiment load = new LoadExperiment();
        load.execute(filepath);
        // register Broadcast receiver for timing of stimuli (notification when stimulus is to be played)
        if(timerTickReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_TIMER);
            registerReceiver(timerTickReceiver, intentFilter);
        }

        // register Broadcast receiver for timing of sequences (notification when one sequence finished)
        if(timerStopReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_TIMER_STOP);
            registerReceiver(timerStopReceiver, intentFilter);
        }

    }*/

    @Override
    public void onBackPressed() {
        boolean allowBackButton = false;
        if (allowBackButton) {
            super.onBackPressed();
        }
    }

}



