package mhealth.eeg_presentation_app.StimuliViewer;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Ina on 09/02/16.
 * Service to notify the presenter activity when a stimuli should be shown
 */
public class StimuliTimer extends Service {
    private long distance; // time distance between 2 stimuli
    private long duration; // time duration of a stimuli
    private int stimuliAmount;
    private long addedTime; // distance + duration
    private boolean training; // set to true when in training phase, false otherwise
    private boolean waitAfterLast; // Wait distance time after presentation (duration) of last stimulus (e.g. in a reaction test the duration a stimulus is on screen is very short and the time between two stimuli relatively long)

    private static final String ACTION_STRING_TIMER = "ToPresenter"; // Broadcast to Presenter activity
    private static final String ACTION_TIMER_STOP = "ToPresenter_Stop"; // Broadcast to Presenter activity when last stimulus of sequence was played

    private Timer timer; // timer

    int usedStimuli = 0;
    // Timer for displaying a new stimulus
    TimerTask stimuliTimer;

    // Timer for displaying no stimuli & signaling that a sequence was finished
    TimerTask breakTimer;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Executed when service starts
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(final Intent intent, int flags, final int startId) {
        super.onStartCommand(intent, flags, startId);
        if(stimuliTimer != null) {
            stimuliTimer.cancel();
        }
        if(breakTimer != null) {
            breakTimer.cancel();
        }
        if(timer != null) {
            timer.cancel();
        }

        // get the duration of a stimuli and the time passing between to stimulus onsets and the amount of stimuli to be shown
        this.distance = (long)intent.getExtras().getInt("distance");
        this.duration = (long)intent.getExtras().getInt("duration");
        this.stimuliAmount = intent.getExtras().getInt("amount");
        this.training = intent.getExtras().getBoolean("training");
        this.waitAfterLast = true;

        stimuliTimer = new TimerTask() {
            @Override
            public void run() {
                Intent intent_presenter = new Intent();
                intent_presenter.putExtra("training", training); // informs presenter whether the presented stimuli are from training phase or not
                if (usedStimuli < stimuliAmount) {
                    intent_presenter.putExtra("stimuli", true); // if stimuli is true, then a stimuli should be shown and nothing otherwise
                    intent_presenter.setAction(ACTION_STRING_TIMER);
                    usedStimuli++;
                } else if (waitAfterLast) {
                    timer.cancel();
                    intent_presenter.setAction(ACTION_TIMER_STOP);
                }
                sendBroadcast(intent_presenter); // send respective broadcast
            }
        };

        breakTimer = new TimerTask() {
            @Override
            public void run() {
                Intent intent_presenter = new Intent();
                intent_presenter.putExtra("training", training); // informs presenter whether the presented stimuli are from training phase or not
                // if there are still stimuli to be shown in this sequence, just signal a break
                if (usedStimuli < stimuliAmount) {
                    intent_presenter.putExtra("stimuli", false); // if stimuli is true, then a stimuli should be shown and nothing otherwise
                    intent_presenter.setAction(ACTION_STRING_TIMER);
                } else if (waitAfterLast) {
                    intent_presenter.putExtra("stimuli", false); // if stimuli is true, then a stimuli should be shown and nothing otherwise
                    intent_presenter.setAction(ACTION_STRING_TIMER);
                }
                else { // if no stimuli remaining, signal end of sequence
                    timer.cancel();
                    intent_presenter.setAction(ACTION_TIMER_STOP);
                }
                sendBroadcast(intent_presenter); // send respective broadcast
            }
        };
        // scheduling the task at fixed rate delay
        timer = new Timer();
        timer.scheduleAtFixedRate(breakTimer, 500+this.duration, this.distance);
        timer.scheduleAtFixedRate(stimuliTimer, 500, this.distance);

        return 1;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stimuliTimer.cancel();
        breakTimer.cancel();
        timer.cancel();
    }

}
