package mhealth.eeg_presentation_app.StimuliViewer.StimuliModules;

/**
 * Created by Ina on 01/04/16.
 */
public class Stimulus {
    private byte[] source;
    private String label;

    public Stimulus(byte[] source, String label) {
        this.source = source;
        this.label = label;
    }

    public byte[] getSource() {
        return this.source;
    }

    public String getLabel() {
        return this.label;
    }
}
