package mhealth.eeg_presentation_app.StimuliViewer.ExperimentModules;

import mhealth.eeg_presentation_app.StimuliViewer.StimuliModules.Stimulus;

/**
 * Created by Ina on 02/02/16.
 * Normal sequence object with standard stimuli
 */
public class Sequence {
    protected String name;
    protected int time_distance; // time distance between different stimuli
    protected boolean random; // random order of stimuli?
    protected boolean ordered; // order of stimuli given?
    protected int repeat_times; // how often should the sequence be repeated?
    protected boolean repeat_newOrder; // new order of stimuli when repeated?
    protected boolean touch;
    protected int type;

    protected boolean hasInstruction;
    protected String instruction;
    protected boolean hasTraining;
    protected int trainingAmount;
    protected int [] trainingOrder;
    protected boolean hasBackground;

    // Feedback for training
    protected int[] trainingTargets;

    protected Stimulus[] sStimuli; // array containing all standard stimuli as byte arrays
    protected int sAmount; // how many times should each of the target stimuli be shown?
    protected int sSingleAmount;
    protected int sDuration; // how long should a standard stimuli be shown?

    protected int sNumber; // number of different standard stimuli
    protected int [] sOrder; // presentation order of the standard stimuli

    protected int s_xpos;
    protected int s_ypos;
    protected String s_textcolor;
    protected int s_textsize;

    public void initStandardStimuli(int number) {
        sStimuli = new Stimulus[number];
        sNumber = number;
    }

    protected void insertStandardStimulus(Stimulus stimulus, int number) {
        this.sStimuli[number] = stimulus;
    }

    protected Stimulus getStandardStimulus(int num) {
       // System.out.println("num: "+num+" size; "+sStimuli.length);
        return sStimuli[num];
    }

}
