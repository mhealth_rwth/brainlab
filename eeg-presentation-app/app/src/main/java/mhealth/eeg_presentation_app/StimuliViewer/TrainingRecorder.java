package mhealth.eeg_presentation_app.StimuliViewer;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

public class TrainingRecorder extends Service {
    // private Handler handler;
    private ArrayList<Long> stimuliTimes; // Stores times of presented stimuli
    private ArrayList<Long> touchEvents; // Stores times of touch events
    private int[] targetList;

    private static final String ACTION_STRING_EVENT = "Event";
    private static final String ACTION_TRAINING_STOP = "ToTrainingRecorder_Stop";
    private static final String ACTION_TRAINING_FEEDBACK = "ToPresenter_Feedback";
    private static final int WAITING_TIME = 1400; // time in ms to wait after training phase is done, to prevent user from skipping feedback screen

    public TrainingRecorder() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Connection to EEG in background, receiving of EEG data packets, handles disconnection of EEG
     */
    public int onStartCommand(Intent intent, int flags, final int startId) {
        Log.d("TrainingRecorder", "onStartCommand called");
        // handler = new Handler();
        super.onStartCommand(intent,flags,startId);
        stimuliTimes = new ArrayList<Long>();
        touchEvents = new ArrayList<Long>();

        targetList = intent.getExtras().getIntArray("targetList");
        Log.d("TrainingRecorder", "onStart. Target list: " + Arrays.toString(targetList));

        // register Receivers

        if(annotationReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_EVENT);
            registerReceiver(annotationReceiver, intentFilter);
        }
        if(trainingFinished != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_TRAINING_STOP);
            registerReceiver(trainingFinished, intentFilter);
            Log.d("TrainingRecorder", "TRAININGFINISHED intent receiver registered");
        }

        return Service.START_STICKY;
    }


    /**
     *  Broadcast to Presenter Activity to inform when saving is done
     */
    private void sendBroadcastToPresenter(int[] feedback) {
        Intent intent_presenter = new Intent();
        intent_presenter.setAction(ACTION_TRAINING_FEEDBACK);
        Log.d("TrainingRecorder", "Send correctTouches array to presenter: " + Arrays.toString(feedback));
        intent_presenter.putExtra("feedback", feedback);
        sendBroadcast(intent_presenter);
    }


    /**
     * BroadcastReceiver to get event annotations
     */
    private BroadcastReceiver annotationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle intent_extras = intent.getExtras();

            new Thread(new Runnable() {
                public void run() {
                    Long curTime = intent_extras.getLong("timestamp") ;
                    String curAnnot = intent_extras.getString("event");

                    if (curAnnot.equals("Touch")) {
                        touchEvents.add(curTime);
                    } else {
                        stimuliTimes.add(curTime);
                    }
                }
            }).start();
        }
    };

    /**
     * BroadcastReceiver to get decoded EEG measurements
     */
    private BroadcastReceiver trainingFinished = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            unregisterReceiver(annotationReceiver);
            Log.d("TrainingRecorder", "trainingFinished broadcast");
            giveFeedback();
        }
    };


    public void giveFeedback() {

        new Thread( new Runnable() {
            public void run() {
                int stimuliAmount = stimuliTimes.size();
                int[] stimuliTouched = new int[stimuliAmount]; // Initialize array with 0s, meaning no stimulus was touched
                for (int i = 0; i < stimuliTouched.length; i++) {
                    stimuliTouched[i] = 0;
                }
                if (!touchEvents.isEmpty()) {
                    int touchAmount = touchEvents.size();
                    Long curTouch;
                    boolean laterThanCurEvent;
                    int s;
                    for (int t = 0; t < touchAmount; t++) {
                        curTouch = touchEvents.get(t);
                        laterThanCurEvent = true;
                        s = 1;
                        while (s < stimuliAmount && laterThanCurEvent) {
                            if (curTouch < stimuliTimes.get(s)) {
                                laterThanCurEvent = false;
                            } else {
                                s++;
                            }
                        }
                        stimuliTouched[s-1] = 1;
                    }
                }

                int[] targetListBin = new int[stimuliAmount]; // Initialize array with 0s, meaning no target at this position
                for (int i = 0; i < targetListBin.length; i++) {
                    targetListBin[i] = 0;
                }
                for (int t = 0; t < targetList.length; t++) { // Set target positions to 1
                    targetListBin[targetList[t]] = 1;
                }

                int numTargetsClicked = 0;
                int numNonTargetsClicked = 0;
                int[] correctTouches = new int[stimuliAmount];
                for (int i = 0; i < correctTouches.length; i++) {
                    if (targetListBin[i] == 1 && stimuliTouched[i] == 0) {
                        correctTouches[i] = 0;
                    } else if (targetListBin[i] == 0 && stimuliTouched[i] == 1) {
                        correctTouches[i] = 0;
                        numNonTargetsClicked++;
                    } else if (targetListBin[i] == 1 && stimuliTouched[i] == 1) {
                        correctTouches[i] = 1;
                        numTargetsClicked++;
                    } else {
                        correctTouches[i] = 1;
                    }
                }

                int numTargets = targetList.length;

                int[] feedback = {numTargets, numTargetsClicked, numNonTargetsClicked};
                try {
                    Thread.sleep(WAITING_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                sendBroadcastToPresenter(feedback);
                stopSelf();
            }
        }).start();

    }


    /**
     * when service is stopped, the BroadcastReceivers have to be unregistered
     */
    public void onDestroy() {
        Log.d("TrainingRecorder", "onDestroy() called");
        try {
            unregisterReceiver(annotationReceiver);
        } catch(IllegalArgumentException e) {
        }
        unregisterReceiver(trainingFinished);
        super.onDestroy();
    }
}
