package mhealth.eeg_presentation_app.StimuliViewer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import mhealth.eeg_presentation_app.MainActivity;
import mhealth.eeg_presentation_app.R;
import mhealth.eeg_presentation_app.SetUp;

public class ExperimentOverview extends AppCompatActivity {
    // how many experiments are available
    private int experiment_amount;
    // create array for presentation files
    private String[] presentations;

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    // BroadcastReceiver
    private static final String ACTION_STRING_ACTIVITY = "ToAll";

    private String filepath;

    Activity act = this;

    /**
     * register to BroadcastReceivers of the emotivService to get data
     * start thread for updating status
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("ExperimentOverview", "onCreate() called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer);

        if(toAllReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_ACTIVITY);
            registerReceiver(toAllReceiver, intentFilter);
        }

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.viewer_layout);
        loadFiles();

        // set OnClickListener
        if(experiment_amount != 0) {
            // get the listview
            expListView = (ExpandableListView) findViewById(R.id.experiment_list);

            // preparing list data
            // prepareListData();

            listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

            // setting list adapter
            expListView.setAdapter(listAdapter);

            // listview on child click listener
            expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v,
                                            int groupPosition, int childPosition, long id) {
                    goToPresenter(filepath + listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition));
                    return false;
                }
            });

        } else {
            Toast.makeText(getApplicationContext(),"Wrong filepath! No files found! Leave field empty to use default path",Toast.LENGTH_LONG).show();
            Intent setup_intent = new Intent(getApplicationContext(), SetUp.class);
            startActivity(setup_intent);
        }

    }


    /**
     * BroadcastReceiver to detect when EEG has been disconnected
     */
    private BroadcastReceiver toAllReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent main_intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(main_intent);
            unregisterReceiver(toAllReceiver);
        }
    };

    private void loadFiles(String path) {
        try{
            File expOverview;
            expOverview = new File(path+"Experiments.xml");
            filepath = path;
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(expOverview);
            doc.getDocumentElement().normalize();

            Element root = (Element) doc.getElementsByTagName("Experiments").item(0);
            NodeList expList = root.getElementsByTagName("Experiment");
            experiment_amount = expList.getLength();
            presentations = new String[experiment_amount];
            for(int i=0; i<presentations.length; i++) {
                presentations[i] = expList.item(i).getTextContent();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadFiles() {
        try{
            File expOverview = new File("storage/emulated/0/Documents/EEG/Experiments.xml");
            filepath = "storage/emulated/0/Documents/EEG/";
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(expOverview);
            doc.getDocumentElement().normalize();

            listDataHeader = new ArrayList<String>();
            listDataChild = new HashMap<String, List<String>>();

            Element root = (Element) doc.getElementsByTagName("Experiments").item(0);
            NodeList categoryList = root.getElementsByTagName("Category");
            int category_amount = categoryList.getLength();
            for(int i=0; i<category_amount; i++) {
                Node category_node = categoryList.item(i);
                Element category_element = (Element) category_node;

                // Adding category header
                String category_name = category_element.getElementsByTagName("Name").item(0).getTextContent();
                listDataHeader.add(category_name);

                // Adding child data
                NodeList expList = category_element.getElementsByTagName("Experiment");
                experiment_amount = expList.getLength();
                List<String> presentations = new ArrayList<String>();
                for(int s=0; s<experiment_amount; s++) {
                    presentations.add(expList.item(s).getTextContent());
                }

                listDataChild.put(listDataHeader.get(i), presentations);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Decodes Image in form of bitmap from Base64 String
     * @param encodedString Base64 String containing the image
     * @return Image as bitmap
     */
    public Bitmap decodeImageString(String encodedString) {
        byte[] imageInBytes = Base64.decode(encodedString, Base64.DEFAULT);
        Bitmap imageDecoded = BitmapFactory.decodeByteArray(imageInBytes, 0, imageInBytes.length);
        return imageDecoded;
    }

    /**
     * Start ExperimentOverview if an experiment is started
     *
     */
    public void goToPresenter(String path) {
        Log.d("ExperimentOverview", "goToPresenter(String path) called");
        Intent intent = new Intent(this, Presenter.class);
        intent.putExtra("filepath", path);
        startActivity(intent);
    }

    public void goToSetup() {
        Intent intent = new Intent(this, SetUp.class);
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        goToSetup();
        return true;
    }


    /**
     * When activity is destroyed, unregister als BroadcastReceivers
     */
    public void onDestroy() {
        Log.d("ExperimentOverview", "onDestroy called");
        super.onDestroy();
        unregisterReceiver(toAllReceiver);
    }

    /**
     * When service stops, unregister all Receivers
     */
    public void onStop() {
        Log.d("ExperimentOverview", "onStop called");
        super.onStop();
    }

    /**
     * When service is restarted, register all Receivers and start updating
     */
    public void onRestart() {
        Log.d("ExperimentOverview", "onRestart() called");
        super.onRestart();

        if(toAllReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_ACTIVITY);
            registerReceiver(toAllReceiver, intentFilter);
        }
        super.onStart();

    }

    @Override
    public void onBackPressed() {
        boolean allowBackButton = false;
        if (allowBackButton) {
            super.onBackPressed();
        } else {
            goToSetup();
        }
    }

}