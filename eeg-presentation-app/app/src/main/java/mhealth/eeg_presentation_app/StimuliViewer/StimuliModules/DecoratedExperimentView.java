package mhealth.eeg_presentation_app.StimuliViewer.StimuliModules;

import android.content.Context;
import android.content.Intent;

/**
 * Created by Ina on 02/02/16.
 */
public abstract class DecoratedExperimentView implements ExperimentView {
    private static final String ACTION_STRING_EVENT = "Event";
    protected ExperimentView expComp;
    private Context context;

    public DecoratedExperimentView(ExperimentView expComp, Context context) {
        this.expComp = expComp;
        this.context = context;
    }

    public void showBackground() {
        this.expComp.showBackground();
    }

    /**
     *  Broadcast to RecordService with annotations
     */
    protected void sendAnnotationBroadcast(long timestamp, String event) {
        Intent intent_recorder = new Intent();
        intent_recorder.putExtra("timestamp",timestamp);
        intent_recorder.putExtra("event",event);
        intent_recorder.setAction(ACTION_STRING_EVENT);
        this.context.sendBroadcast(intent_recorder);
    }
    /**
     *  Broadcast to RecordService with annotations
     */
    protected void sendAnnotationBroadcast(long timestamp, String event, int duration) {
        Intent intent_recorder = new Intent();
        intent_recorder.putExtra("timestamp",timestamp);
        intent_recorder.putExtra("event",event);
        intent_recorder.putExtra("duration",duration);
        intent_recorder.setAction(ACTION_STRING_EVENT);
        this.context.sendBroadcast(intent_recorder);
    }
}
