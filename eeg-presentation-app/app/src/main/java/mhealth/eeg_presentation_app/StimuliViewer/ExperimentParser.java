package mhealth.eeg_presentation_app.StimuliViewer;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import mhealth.eeg_presentation_app.StimuliViewer.ExperimentModules.Experiment;
import mhealth.eeg_presentation_app.StimuliViewer.StimuliModules.Stimulus;

/**
 * Created by Ina on 25/01/16.
 */
public class ExperimentParser {
    private File exp_file;
    private Experiment exp;
    Context context;

    public ExperimentParser(File file, Context context) {
       // Toast.makeText(context,"Parsing",Toast.LENGTH_LONG).show();
        this.exp_file = file;
        createExperiment(this.exp_file);
        this.context = context;
    }

    public Experiment getExperiment() {
        return this.exp;
    }

    private void createExperiment(File file) {
        try {
            // parse file to XML
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(exp_file);
            doc.getDocumentElement().normalize();
            // get root node
            NodeList nodeList = doc.getElementsByTagName("Experiment");
            Node root = nodeList.item(0);
            Element root_element = (Element) root;
            // get experiment name and create new Experiment object
            String exp_name = root_element.getElementsByTagName("Name").item(0).getTextContent();
            // get SequenceList
            NodeList sequenceList = root_element.getElementsByTagName("Sequence");
            int seq_amount = sequenceList.getLength();
            // create new experiment with seq_amount sequences
            exp = new Experiment(exp_name, seq_amount);
            boolean exp_random = Boolean.parseBoolean(root_element.getElementsByTagName("Random").item(0).getTextContent());
            exp.setRandom(exp_random);
            // boolean exp_ordered = false;
            if (!exp_random && root_element.getElementsByTagName("SeqOrder").getLength() != 0) { // if order is given, set order for presenting stimuli
                Log.w("dPARSER", "Next: Set exp order");
                this.exp.setExpOrder(root_element.getElementsByTagName("SeqOrder").item(0).getTextContent());
                // exp_ordered = true;
            }
            int seq_amount_repeated = seq_amount;
            // go through all possible Sequences of the Experiment and get the according attributes & stimuli
            for(int i=0; i<seq_amount;i++) {
                // seq_elem is the current sequence
                Node cur_seq = sequenceList.item(i);
                Element seq_elem = (Element) cur_seq;
                String seq_name = Integer.toString(i+1); // set sequence name to sequence number if not specified
                // get sequence name, if specified
                if (seq_elem.getElementsByTagName("Name").getLength() != 0) { // if order is given, set order for presenting stimuli
                    seq_name = seq_elem.getElementsByTagName("Name").item(0).getTextContent();
                }
                // get sequence type, duration, timedistance between stimuli, if they are to be presented randomly and how often the sequence should be repeated
                String seq_type = seq_elem.getElementsByTagName("Type").item(0).getTextContent().toLowerCase();
                int type = 0;
                if(seq_type.equals("standard") || seq_type.equals("oddball")) {
                    if(seq_type.equals("standard")) {
                        type = 0;
                    } else if(seq_type.equals("oddball") && ((Element)seq_elem.getElementsByTagName("StandardStimuli").item(0)).getElementsByTagName("Target").getLength() == 0 && ((Element)seq_elem.getElementsByTagName("DeviantStimuli").item(0)).getElementsByTagName("Target").getLength() == 0) {
                        type = 1;
                    } else {
                        type = 2;
                    }
                } else {
                    Toast.makeText(context,"Error: No standard or oddball experiment",Toast.LENGTH_LONG).show();
                }
                // create new sequence object, it is automatically chosen as oddball or standard sequence
                this.exp.createSeq(seq_name, type, i);
                // Is there a fixation cross background during presentation of the sequence stimuli?
                if (seq_elem.getElementsByTagName("Background").getLength() != 0) {
                    exp.setBackground(Boolean.parseBoolean(seq_elem.getElementsByTagName("Background").item(0).getTextContent()), i);
                } else {
                    exp.setBackground(false, i);
                }
                // Is there an instruction presented previous to the presented sequence?
                if (seq_elem.getElementsByTagName("Instruction").getLength() != 0) {
                    exp.setInstruction(true, seq_elem.getElementsByTagName("Instruction").item(0).getTextContent(), i);
                } else {
                    exp.setInstruction(false, "", i);
                }
                // get all important attributes belonging to the sequence
                this.exp.setSeqDistance(Integer.parseInt(seq_elem.getElementsByTagName("TimeDistance").item(0).getTextContent()), i);
                boolean seq_random = Boolean.parseBoolean(seq_elem.getElementsByTagName("Random").item(0).getTextContent());
                this.exp.setSeqRandom(seq_random, i);
                boolean stimuli_ordered = false;
                if (!seq_random && seq_elem.getElementsByTagName("Order").getLength() != 0) { // if order is given, set order for presenting stimuli
                    this.exp.setSeqOrder(seq_elem.getElementsByTagName("Order").item(0).getTextContent(), i);
                    stimuli_ordered = true;
                }
                if (seq_elem.getElementsByTagName("Training").getLength() != 0) { // if training sequence is given, set order for training mode
                    this.exp.setTraining(true, seq_elem.getElementsByTagName("Training").item(0).getTextContent(), seq_elem.getElementsByTagName("TrainingTargets").item(0).getTextContent(), i);
                } else {
                    this.exp.setTraining(false, "", "", i);
                }
                this.exp.setSeqRepeatTimes(Integer.parseInt(seq_elem.getElementsByTagName("Repeat").item(0).getTextContent()), i);
                seq_amount_repeated += Integer.parseInt(seq_elem.getElementsByTagName("Repeat").item(0).getTextContent());
                this.exp.setSeqTouch(Boolean.parseBoolean(seq_elem.getElementsByTagName("Touch").item(0).getTextContent()), i);

                // find out of which kind the stimuli are
                switch (seq_elem.getElementsByTagName("Stimulitype").item(0).getTextContent()) {
                    case "Image":
                        this.exp.setSeqStimuliType(0, i);
                        break;
                    case "Audio":
                        this.exp.setSeqStimuliType(1, i);
                        break;
                    case "Text":
                        this.exp.setSeqStimuliType(2, i);
                        break;
                    default:
                        Toast.makeText(context,"Error: Use image, audio or text stimuli",Toast.LENGTH_LONG).show();
                        break;
                }

                // get all standard stimuli and how often & long they should be presented
                Node standardStimuli = seq_elem.getElementsByTagName("StandardStimuli").item(0);
                Element standard_elem = (Element) standardStimuli;

/*                if (exp.getSeqStimuliType(i) == 0 || exp.getSeqStimuliType(i) == 2) { // if image or text stimulus, we need additional paramters as when we used audio
                    if (standard_elem.getElementsByTagName("Xpos").item(0) != null && standard_elem.getElementsByTagName("Ypos").item(0) != null) {
                        this.exp.setSeqXpos(Integer.parseInt(standard_elem.getElementsByTagName("Xpos").item(0).getTextContent()), i);
                        this.exp.setSeqYpos(Integer.parseInt(standard_elem.getElementsByTagName("Ypos").item(0).getTextContent()), i);
                    } else {
                        this.exp.setSeqXpos(-1, i);
                        this.exp.setSeqYpos(-1, i);
                    }
                }*/
                Element cur_stimuli;
                if (standard_elem.getElementsByTagName("Target").getLength() != 0) { // Are there standard target stimuli available?
                    cur_stimuli = (Element) standard_elem.getElementsByTagName("Target").item(0);
                } else {
                    cur_stimuli = standard_elem;
                }
                this.exp.setSeqStandardDuration(Integer.parseInt(cur_stimuli.getElementsByTagName("Duration").item(0).getTextContent()), i);
                if (!stimuli_ordered) {
                    if (cur_stimuli.getElementsByTagName("Amount").getLength() != 0) {
                        this.exp.setSeqStandardAmount(Integer.parseInt(cur_stimuli.getElementsByTagName("Amount").item(0).getTextContent()), i);
                        this.exp.setSeqStandardSingleAmount(0, i);
                    } else {
                        this.exp.setSeqStandardAmount(0, i);
                        if (cur_stimuli.getElementsByTagName("SingleAmount").getLength() != 0) {
                            this.exp.setSeqStandardSingleAmount(Integer.parseInt(cur_stimuli.getElementsByTagName("SingleAmount").item(0).getTextContent()), i);
                        } else {
                            Toast.makeText(context,"Error: False amount of stimuli",Toast.LENGTH_LONG).show();
                        }
                    }
                }

                NodeList trialList = cur_stimuli.getElementsByTagName("Trial"); // Target stimuli list
                int trial_amount = trialList.getLength();
                this.exp.initSeqStandard(trial_amount, i);


                if (exp.getSeqStimuliType(i) == 0 || exp.getSeqStimuliType(i) == 1) { // if image or audio, decode Base64 source code
                    for (int n = 0; n < trial_amount; n++) {
                        Element label = ((Element) ((Element) trialList.item(n)).getElementsByTagName("Label").item(0));
                        byte[] sourcecode = Base64.decode(((Element) trialList.item(n)).getElementsByTagName("Stimulus").item(0).getTextContent(), Base64.DEFAULT);
                        this.exp.insertSeqStandardStimuli(new Stimulus(sourcecode, label.getTextContent()), n, i);
                    }
                } else { // if text, save string as bytes
                    this.exp.setSeqsTextColor(cur_stimuli.getElementsByTagName("Color").item(0).getTextContent(), i); // TODO color and size attributes
                    this.exp.setSeqsTextSize(Integer.parseInt(cur_stimuli.getElementsByTagName("Size").item(0).getTextContent()), i);
                    for (int n = 0; n < trial_amount; n++) {
                        Element label = ((Element) ((Element) trialList.item(n)).getElementsByTagName("Label").item(0));
                        byte[] sourcecode = ((Element) trialList.item(n)).getElementsByTagName("Stimulus").item(0).getTextContent().getBytes();
                        this.exp.insertSeqStandardStimuli(new Stimulus(sourcecode,label.getTextContent()), n, i);
                    }

                }

                if (this.exp.isOddball(i)) {
                    if(this.exp.isNontarget(i)) {
                        /////////// STANDARD NONTARGET
                        if (standard_elem.getElementsByTagName("Nontarget").getLength() != 0) { // are there also standard nontarget stimuli?
                            cur_stimuli = (Element) standard_elem.getElementsByTagName("Nontarget").item(0);
                            this.exp.setSeqStandardNontargetDuration(Integer.parseInt(cur_stimuli.getElementsByTagName("Duration").item(0).getTextContent()), i);
                            if (cur_stimuli.getElementsByTagName("Amount").getLength() != 0) {
                                this.exp.setSeqStandardNontargetAmount(Integer.parseInt(cur_stimuli.getElementsByTagName("Amount").item(0).getTextContent()), i);
                                this.exp.setSeqStandardNontargetSingleAmount(0, i);
                            } else {
                                this.exp.setSeqStandardNontargetAmount(0, i);
                                if (cur_stimuli.getElementsByTagName("SingleAmount").getLength() != 0) {
                                    this.exp.setSeqStandardNontargetSingleAmount(Integer.parseInt(cur_stimuli.getElementsByTagName("SingleAmount").item(0).getTextContent()), i);
                                } else {
                                    Toast.makeText(context, "Error: False amount of stimuli", Toast.LENGTH_LONG).show();
                                }
                            }

                            trialList = cur_stimuli.getElementsByTagName("Trial"); // Target stimuli list
                            trial_amount = trialList.getLength();
                            this.exp.initSeqStandardNontarget(trial_amount, i);


                            if (exp.getSeqStimuliType(i) == 0 || exp.getSeqStimuliType(i) == 1) { // if image or audio, decode Base64 source code
                                for (int n = 0; n < trial_amount; n++) {
                                    Element label = ((Element) ((Element) trialList.item(n)).getElementsByTagName("Label").item(0));
                                    byte[] sourcecode = Base64.decode(((Element) trialList.item(n)).getElementsByTagName("Stimulus").item(0).getTextContent(), Base64.DEFAULT);
                                    this.exp.insertSeqStandardNontargetStimuli(new Stimulus(sourcecode, label.getTextContent()), n, i);
                                }
                            } else { // if text, save string as bytes
                                this.exp.setSeqsnTextColor(cur_stimuli.getElementsByTagName("Color").item(0).getTextContent(), i); // TODO color and size attributes
                                this.exp.setSeqsnTextSize(Integer.parseInt(cur_stimuli.getElementsByTagName("Size").item(0).getTextContent()), i);
                                for (int n = 0; n < trial_amount; n++) {
                                    Element label = ((Element) ((Element) trialList.item(n)).getElementsByTagName("Label").item(0));
                                    byte[] sourcecode = ((Element) trialList.item(n)).getElementsByTagName("Stimulus").item(0).getTextContent().getBytes();
                                    this.exp.insertSeqStandardNontargetStimuli(new Stimulus(sourcecode, label.getTextContent()), n, i);
                                }

                            }
                        }
                    }

                    //////// DEVIANT
                    Node deviantStimuli = seq_elem.getElementsByTagName("DeviantStimuli").item(0);
                    Element deviant_elem = (Element) deviantStimuli;

                    if (standard_elem.getElementsByTagName("Target").getLength() != 0) { // Are there standard target stimuli available?
                        cur_stimuli = (Element) deviant_elem.getElementsByTagName("Target").item(0);
                    } else {
                        cur_stimuli = deviant_elem;
                    }
                    this.exp.setSeqDeviantDuration(Integer.parseInt(cur_stimuli.getElementsByTagName("Duration").item(0).getTextContent()), i);
                    if (cur_stimuli.getElementsByTagName("Amount").getLength() != 0) {
                        this.exp.setSeqDeviantAmount(Integer.parseInt(cur_stimuli.getElementsByTagName("Amount").item(0).getTextContent()), i);
                        this.exp.setSeqDeviantSingleAmount(0, i);
                    } else {
                        this.exp.setSeqDeviantAmount(0, i);
                        if (cur_stimuli.getElementsByTagName("SingleAmount").getLength() != 0) {
                            this.exp.setSeqDeviantSingleAmount(Integer.parseInt(cur_stimuli.getElementsByTagName("SingleAmount").item(0).getTextContent()), i);
                        } else {
                            Toast.makeText(context,"Error: False amount of stimuli",Toast.LENGTH_LONG).show();
                        }
                    }

                    trialList = cur_stimuli.getElementsByTagName("Trial"); // Target stimuli list
                    trial_amount = trialList.getLength();
                    this.exp.initSeqDeviant(trial_amount, i);


                    if (exp.getSeqStimuliType(i) == 0 || exp.getSeqStimuliType(i) == 1) { // if image or audio, decode Base64 source code
                        for (int n = 0; n < trial_amount; n++) {
                            Element label = ((Element) ((Element) trialList.item(n)).getElementsByTagName("Label").item(0));
                            byte[] sourcecode = Base64.decode(((Element) trialList.item(n)).getElementsByTagName("Stimulus").item(0).getTextContent(), Base64.DEFAULT);
                            this.exp.insertSeqDeviantStimuli(new Stimulus(sourcecode, label.getTextContent()), n, i);
                        }
                    } else { // if text, save string as bytes
                        this.exp.setSeqdTextColor(cur_stimuli.getElementsByTagName("Color").item(0).getTextContent(), i); // TODO color and size attributes
                        this.exp.setSeqdTextSize(Integer.parseInt(cur_stimuli.getElementsByTagName("Size").item(0).getTextContent()), i);
                        for (int n = 0; n < trial_amount; n++) {
                            Element label = ((Element) ((Element) trialList.item(n)).getElementsByTagName("Label").item(0));
                            byte[] sourcecode = ((Element) trialList.item(n)).getElementsByTagName("Stimulus").item(0).getTextContent().getBytes();
                            this.exp.insertSeqDeviantStimuli(new Stimulus(sourcecode, label.getTextContent()), n, i);
                        }

                    }

                    if(this.exp.isNontarget(i)) {
                        //////// DEVIANT NONTARGET
                        if (deviant_elem.getElementsByTagName("Nontarget").getLength() != 0) { // are there also standard nontarget stimuli?
                            cur_stimuli = (Element) deviant_elem.getElementsByTagName("Nontarget").item(0);
                            this.exp.setSeqDeviantNontargetDuration(Integer.parseInt(cur_stimuli.getElementsByTagName("Duration").item(0).getTextContent()), i);
                            if (cur_stimuli.getElementsByTagName("Amount").getLength() != 0) {
                                this.exp.setSeqDeviantNontargetAmount(Integer.parseInt(cur_stimuli.getElementsByTagName("Amount").item(0).getTextContent()), i);
                                this.exp.setSeqDeviantNontargetSingleAmount(0, i);
                            } else {
                                this.exp.setSeqDeviantNontargetAmount(0, i);
                                if (cur_stimuli.getElementsByTagName("SingleAmount").getLength() != 0) {
                                    this.exp.setSeqDeviantNontargetSingleAmount(Integer.parseInt(cur_stimuli.getElementsByTagName("SingleAmount").item(0).getTextContent()), i);
                                } else {
                                    Toast.makeText(context, "Error: False amount of stimuli", Toast.LENGTH_LONG).show();
                                }
                            }

                            trialList = cur_stimuli.getElementsByTagName("Trial"); // Target stimuli list
                            trial_amount = trialList.getLength();
                            this.exp.initSeqDeviantNontarget(trial_amount, i);


                            if (exp.getSeqStimuliType(i) == 0 || exp.getSeqStimuliType(i) == 1) { // if image or audio, decode Base64 source code
                                for (int n = 0; n < trial_amount; n++) {
                                    Element label = ((Element) ((Element) trialList.item(n)).getElementsByTagName("Label").item(0));
                                    byte[] sourcecode = Base64.decode(((Element) trialList.item(n)).getElementsByTagName("Stimulus").item(0).getTextContent(), Base64.DEFAULT);
                                    this.exp.insertSeqDeviantNontargetStimuli(new Stimulus(sourcecode, label.getTextContent()), n, i);
                                }
                            } else { // if text, save string as bytes
                                this.exp.setSeqdnTextColor(cur_stimuli.getElementsByTagName("Color").item(0).getTextContent(), i); // TODO color and size attributes
                                this.exp.setSeqdnTextSize(Integer.parseInt(cur_stimuli.getElementsByTagName("Size").item(0).getTextContent()), i);
                                for (int n = 0; n < trial_amount; n++) {
                                    Element label = ((Element) ((Element) trialList.item(n)).getElementsByTagName("Label").item(0));
                                    byte[] sourcecode = ((Element) trialList.item(n)).getElementsByTagName("Stimulus").item(0).getTextContent().getBytes();
                                    this.exp.insertSeqDeviantNontargetStimuli(new Stimulus(sourcecode, label.getTextContent()), n, i);
                                }

                            }
                        }
                    }
                }
            }
            this.exp.setSequenceAmountRepeated(seq_amount_repeated); // how many sequences including repetitions
            Log.w("dPARSER", "Parsing finished");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
