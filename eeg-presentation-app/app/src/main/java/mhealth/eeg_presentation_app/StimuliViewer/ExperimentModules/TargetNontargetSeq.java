package mhealth.eeg_presentation_app.StimuliViewer.ExperimentModules;

import mhealth.eeg_presentation_app.StimuliViewer.StimuliModules.Stimulus;

/**
 * Created by Ina on 01/04/16.
 */
public class TargetNontargetSeq extends OddballSeq {
    protected Stimulus[] snStimuli; // array containing all nontargetStandardstimuli as byte arrays
    protected int snAmount; // how many times should each of the target stimuli be shown?
    protected int snSingleAmount;
    protected int snDuration; // how long should a nontargetStandardstimuli be shown?
    protected int snNumber; // number of different nontargetStandardstimuli

    protected Stimulus[] dnStimuli; // array containing all nontargetDeviant stimuli as byte arrays
    protected int dnAmount; // how many times should each of the target stimuli be shown?
    protected int dnSingleAmount;
    protected int dnDuration; // how long should a nontargetDeviant stimuli be shown?
    protected int dnNumber; // number of different nontargetDeviant stimuli


    protected int sn_xpos;
    protected int sn_ypos;
    protected String sn_textcolor;
    protected int sn_textsize;

    protected int dn_xpos;
    protected int dn_ypos;
    protected String dn_textcolor;
    protected int dn_textsize;


    public void initStandardNontargetStimuli(int number) {
        snStimuli = new Stimulus[number];
        snNumber = number;
    }

    protected void insertStandardNontargetStimulus(Stimulus stimulus, int number) {
        this.snStimuli[number] = stimulus;
    }

    protected Stimulus getStandardNontargetStimulus(int num) {
        return snStimuli[num];
    }

    public void initDeviantNontargetStimuli(int number) {
        dnStimuli = new Stimulus[number];
        dnNumber = number;
    }

    protected void insertDeviantNontargetStimulus(Stimulus stimulus, int number) {
        this.dnStimuli[number] = stimulus;
    }

    protected Stimulus getDeviantNontargetStimulus(int num) {
        return dnStimuli[num];
    }

}