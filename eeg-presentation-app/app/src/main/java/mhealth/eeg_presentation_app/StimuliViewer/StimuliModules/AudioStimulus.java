package mhealth.eeg_presentation_app.StimuliViewer.StimuliModules;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Class to play an audio stimuli
 * Created by Ina on 02/22/16.
 */
public class AudioStimulus extends DecoratedExperimentView {
    private MediaPlayer mediaPlayer = null;
    private String label;

    public AudioStimulus(Context context, ExperimentView expComp, Stimulus stimulus) {
        super(expComp, context);

        // read byte array with audio file into MediaPlayer
        try{
            File tempAudio = File.createTempFile("tone","wav");
            tempAudio.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(tempAudio);
            fos.write(stimulus.getSource());
            fos.close();
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            FileInputStream fis = new FileInputStream(tempAudio);
            mediaPlayer.setDataSource(fis.getFD());
            mediaPlayer.prepare();
            label = stimulus.getLabel();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    public AudioStimulus(Context context, ExperimentView expComp, ImageView view) {
        super(expComp, context);
    }

    public void showBackground() {
        super.showBackground();
    }

    /**
     * play given tone
     */
    public void playTone() {
        mediaPlayer.start();
        super.sendAnnotationBroadcast(System.currentTimeMillis(),label);
    }

    /**
     * stop MediaPlayer immediately
     */
    public void abortTone() {
        if(mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
        }
    }
}

