package mhealth.eeg_presentation_app.StimuliViewer.ExperimentModules;

import mhealth.eeg_presentation_app.StimuliViewer.StimuliModules.Stimulus;

/**
 * Created by Ina on 02/02/16.
 * Extended Sequence object which contains also deviant stimuli
 */
public class OddballSeq extends Sequence {
    protected Stimulus[] dStimuli; // array containing all deviant stimuli as byte arrays
    protected int dAmount; // how many times should each of the target stimuli be shown?
    protected int dSingleAmount;
    protected int dDuration; // how long should a deviant stimuli be shown?

    protected int dNumber; // number of different deviant stimuli

    protected int d_xpos;
    protected int d_ypos;
    protected String d_textcolor;
    protected int d_textsize;

    public void initDeviantStimuli(int number) {
        dStimuli = new Stimulus[number];
        dNumber = number;
    }

    protected void insertDeviantStimulus(Stimulus stimulus, int number) {
        this.dStimuli[number] = stimulus;
    }

    protected Stimulus getDeviantStimulus(int num) {
        return dStimuli[num];
    }

}
