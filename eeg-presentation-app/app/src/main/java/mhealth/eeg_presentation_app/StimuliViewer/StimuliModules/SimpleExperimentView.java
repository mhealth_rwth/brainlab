package mhealth.eeg_presentation_app.StimuliViewer.StimuliModules;

import android.graphics.Color;
import android.widget.RelativeLayout;

/**
 * Created by Ina on 02/02/16.
 */
public class SimpleExperimentView implements ExperimentView {
    private RelativeLayout layout;

    public SimpleExperimentView(RelativeLayout lay) {
        this.layout = lay;
    }

    // set background color to black
    public void showBackground() {
        layout.setBackgroundColor(Color.LTGRAY);
    } // BLACK

}
