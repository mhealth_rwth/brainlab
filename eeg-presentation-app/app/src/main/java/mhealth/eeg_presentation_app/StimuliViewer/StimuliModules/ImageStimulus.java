package mhealth.eeg_presentation_app.StimuliViewer.StimuliModules;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import mhealth.eeg_presentation_app.MyGLRenderer;

/**
 * Class to display an image stimuli
 * Created by Ina on 02/02/16.
 */
public class ImageStimulus extends DecoratedExperimentView {
    private MyGLRenderer renderer;
    private Bitmap bitmap;
    private String label;

    int xpos = -1;
    int ypos = -1;

    public ImageStimulus(Context context, ExperimentView expComp, MyGLRenderer renderer, Stimulus stimulus) {
        super(expComp, context);
        this.bitmap = BitmapFactory.decodeByteArray(stimulus.getSource(), 0, stimulus.getSource().length); // generate bitmap from source code
        this.label = stimulus.getLabel();
        this.renderer = renderer;
    }

    public ImageStimulus(Context context, ExperimentView expComp, MyGLRenderer renderer) {
        super(expComp, context);
    }

    public void showBackground() {
        super.showBackground();
    }

    /**
     * show given image
     */
    public void displayImage() {
        renderer.setBitmap(bitmap, label);
    }

}

