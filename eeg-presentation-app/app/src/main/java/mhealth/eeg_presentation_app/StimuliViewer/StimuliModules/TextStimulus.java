package mhealth.eeg_presentation_app.StimuliViewer.StimuliModules;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

import java.nio.charset.Charset;

/**
 * Class to play an audio stimuli
 * Created by Ina on 23/22/16.
 */
public class TextStimulus extends DecoratedExperimentView {
    String stimulus = null;
    TextView textView = null;
    String label;
    int size;
    int xpos;
    int ypos;
    String color;

    public TextStimulus(Context context, ExperimentView expComp, TextView textView, Stimulus stimulus, int size, String color) {
        super(expComp, context);
        this.textView = textView;
        this.stimulus = new String(stimulus.getSource(), Charset.defaultCharset());
        this.label = stimulus.getLabel();
        this.size = size;
        this.color = color;
    }

    public TextStimulus(Context context, ExperimentView expComp) {
        super(expComp, context);
    }

    public void showBackground() {
        super.showBackground();
    }

    /**
     * play given tone
     */
    public void showText() {
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        switch(color.toLowerCase()) {
            case "blue": textView.setTextColor(Color.BLUE);
                break;
            case "black":textView.setTextColor(Color.BLACK);
                break;
            case "yellow":textView.setTextColor(Color.YELLOW);
                break;
            case "cyan":textView.setTextColor(Color.CYAN);
                break;
            case "green":textView.setTextColor(Color.GREEN);
                break;
            case "white":textView.setTextColor(Color.WHITE);
                break;
            case "magenta":textView.setTextColor(Color.MAGENTA);
                break;
            case "gray":textView.setTextColor(Color.GRAY);
                break;
            case "red":textView.setTextColor(Color.RED);
                break;
            default:textView.setTextColor(Color.WHITE);
                break;
        }
        super.sendAnnotationBroadcast(System.currentTimeMillis(), label);
        textView.setText(stimulus);
        /*if(xpos >= 0) {
            textView.setX((float)xpos);
        } else {
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        if(ypos >= 0) {
            textView.setY((float)ypos);
        } else {
            textView.setGravity(Gravity.CENTER_VERTICAL);
        }*/
    }

}

