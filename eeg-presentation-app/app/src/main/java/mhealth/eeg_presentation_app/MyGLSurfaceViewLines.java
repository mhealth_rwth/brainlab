package mhealth.eeg_presentation_app;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by Ina on 22/03/16.
 */
public class MyGLSurfaceViewLines extends MyGLSurfaceViewBasic {

    public MyGLSurfaceViewLines(Context context) {
        super(context, new MyGLRendererLines(context));
    }
    public MyGLSurfaceViewLines(Context context, AttributeSet attrs) {
        super(context, attrs,new MyGLRendererLines(context));
        // Create an OpenGL ES 1.0 context
    }

}
