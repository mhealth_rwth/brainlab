package mhealth.eeg_presentation_app.DemoViewer;

/**
 * Created by Ina on 09/01/16.
 * Measurement object (EEG signal + timestamp)
 */
public class Measurement {
    private int value;
    private Long timestamp;
    private int quality;

    public Measurement(int val, Long time, int quality) {
        this.value = val;
        this.timestamp = time;
        this.quality = quality;
    }

    public int getValue() {
        return this.value;
    }

    public Long getTimestamp() {
        return this.timestamp;
    }

    public int getQuality() {
        return this.quality;
    }

}
