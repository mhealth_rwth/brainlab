package mhealth.eeg_presentation_app.DemoViewer;

/**
 * Created by Ina on 28/03/16.
 */
public class Annotation {
    private long timestamp;
    private String event;
    private int duration;

    public Annotation(long timestamp, String event) {
        this.timestamp = timestamp;
        this.event = event;
    }

    public Annotation(long timestamp, String event, int duration) {
        this.timestamp = timestamp;
        this.event = event;
        this.duration = duration;
    }

    public String getEvent() {
        return event;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getDuration() {
        return duration;
    }


}
