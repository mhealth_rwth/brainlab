package mhealth.eeg_presentation_app.DemoViewer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import mhealth.eeg_presentation_app.MainActivity;
import mhealth.eeg_presentation_app.MyGLRendererLines;
import mhealth.eeg_presentation_app.MyGLSurfaceViewLines;
import mhealth.eeg_presentation_app.R;
import mhealth.eeg_presentation_app.Recorder.RecordService;

public class DemoMeasuring extends AppCompatActivity {
    private static final String ACTION_STRING_ACTIVITY = "ToAll";
    private static final String ACTION_STRING_MEASURING = "ToMeasuringUsers";
    private static final String ACTION_STRING_STOPRECORDING = "ToRecorder";
    private static final String ACTION_STRING_EVENT = "Event";



    private ArrayList<FloatBuffer> measurements;


    // variable for timestamps
    private static long received_time = 0;

    // Electrode names
    private String[] electrodes = {"F3", "FC5", "AF3", "F7", "T7", "P7", "O1", "O2", "P8", "T8", "F8", "AF4", "FC6", "F4"};
    private int curPos;
    private float[] yOffsets;

    int maxPixels;


    /**
     * Register to all needed BroadcastReceivers
     * toAllReceiver = detect when EEG was disconnected
     * demoDataReceiver = get decoded EEG measurements
     *
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_measuring2);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        measurements = new ArrayList<FloatBuffer>();
        yOffsets = new float[electrodes.length];
        for(int i=0; i<yOffsets.length; i++) {
            // multiply with 2 because range is -1 to 1. then subtract 1 to shift graph back into range -1 to 1
            // yOffsets.length+1f because we want three lines per graph and the last graph should not be
            // under the screen
            yOffsets[i] = i*(2f/(yOffsets.length + 1f)) -1f;
        }

        Point size = new Point();
        getWindowManager().getDefaultDisplay().getSize(size);
        // max pixels of screen height and width
        maxPixels = Math.max(size.x, size.y);
        // for each electrode
        for(int i=0; i<electrodes.length; i++) {
            // create as many floats as 4*maxPixels
            ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4 * maxPixels * Float.SIZE / 8);
            byteBuffer.order(ByteOrder.nativeOrder());
            FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
            // save floatbuffer with 4*maxPixels floats in measurements
            measurements.add(floatBuffer);
            /*
            We want to draw lines with openGL. we draw one line between each point. Therefore, we use two coordinates
            for each pixel. Thus, we need 4 values per pixel.
            1. value = x of first point
            2. value = y of first point
            3. value = x of second point
            4. value = y of second point
             */
            for(int j=0; j<maxPixels; j++) {
                int pos = 4*j;
                // for floatbuffer of current electrode
                // put in x value of first pixel = pixel
                measurements.get(i).put(pos,-1f + 2f*((float)(j)/(float)(maxPixels))); // x value of first pixel
                // put in x value of second pixel = pixel+1
                measurements.get(i).put(pos+2, -1f + 2f*((float)(j+1)/(float)(maxPixels))); // x value of next pixel

                if (j<=250) {
                    measurements.get(i).put(pos+1, 5000f); // black line x value of first pixel
                    measurements.get(i).put(pos+3, 5000f);
                } else {
                    measurements.get(i).put(pos+1, 0f+yOffsets[i]); // y value of first pixel = offset of graph
                    measurements.get(i).put(pos+3, 0f+yOffsets[i]); // y value of next pixel = offset of graph
                }


            }
        }

        MyGLSurfaceViewLines glView = (MyGLSurfaceViewLines) findViewById(R.id.GlViewLines);
        ((MyGLRendererLines)glView.getRenderer()).setMeasurements(measurements);

        // register Receivers to gain data
        if (toAllReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_ACTIVITY);
            registerReceiver(toAllReceiver, intentFilter);
        }
        if(demoDataReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_MEASURING);
            registerReceiver(demoDataReceiver, intentFilter);
        }

        Intent service_intent = new Intent(getApplicationContext(), RecordService.class);
        startService(service_intent);
        Log.d("DemoMeasuring", "RecordService started");


    }

    /**
     * BroadcastReceiver to detect if EEG has been disconnected
     */
    private BroadcastReceiver toAllReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent main_intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(main_intent);
            unregisterReceiver(toAllReceiver);
        }
    };

    /**
     * BroadcastReceiver to get decoded EEG measurements
     */
    private BroadcastReceiver demoDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle intent_extras = intent.getExtras();
            received_time = intent_extras.getLong("received_time");

            // get current measurement for each channel
            for(int i=0; i<electrodes.length; i++) {
                float lastVal;
                if(curPos == 0) { // if curPos 0, get last y value
                    lastVal = measurements.get(i).get(maxPixels*4-1);
                } else { // if curPos != 0, get y value from curPos-1
                    lastVal = measurements.get(i).get(curPos-1);
                }
                // new measurement
                float curVal = ((float) intent_extras.getInt(electrodes[i]) - 8192f) / 8192f / electrodes.length + yOffsets[i];
                synchronized (measurements) {
                    // define y positions of line
                    measurements.get(i).put(curPos + 1, lastVal); // curPos = x and curPos+1 = y of old
                    // scaled down to -1 to 1
                    measurements.get(i).put(curPos + 3, curVal); // curPos+2 = x and curPos+3 = y of new
                    measurements.get(i).put((curPos + 249) % (4 * maxPixels), 5000f); // black line
                    measurements.get(i).put((curPos + 251) % (4 * maxPixels), 5000f);
                }

            }
            //next position
            curPos = (curPos+4) % (4*maxPixels);

            MyGLSurfaceViewLines glView = (MyGLSurfaceViewLines) findViewById(R.id.GlViewLines);
            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                ((MyGLRendererLines)glView.getRenderer()).setRotated(true);
            }

        }
    };


    /**
     * When service is destroyed, unregister all Receivers
     */
    public void onDestroy() {
        super.onDestroy();
        // stop updating of graphs and deregister Receivers
        unregisterReceiver(toAllReceiver);
        unregisterReceiver(demoDataReceiver);
    }

    /**
     * When service stops, unregister all Receivers
     */
    public void onStop() {
        sendBroadcastToRecorder();
        //super.onStop();
        Intent service_intent = new Intent(getApplicationContext(), RecordService.class);
        //stopService(service_intent);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onStop();
}

    /**
     *  Broadcast to SetUp Activity with specific data
     */
    private void sendBroadcastToRecorder() {
        sendAnnotationBroadcast(System.currentTimeMillis(), "StopRecord");

        Intent intent_recorder = new Intent();
        intent_recorder.putExtra("finished", true);
        intent_recorder.setAction(ACTION_STRING_STOPRECORDING);
        sendBroadcast(intent_recorder);
        Log.d("DemoMeasuring", "sendBroadCastToRecorder called.");
    }


    /**
     *  Broadcast to RecordService with annotations
     */
    private void sendAnnotationBroadcast(long timestamp, String event) {
        Intent intent_recorder = new Intent();
        intent_recorder.putExtra("timestamp",timestamp);
        intent_recorder.putExtra("event", event);
        intent_recorder.setAction(ACTION_STRING_EVENT);
        getApplicationContext().sendBroadcast(intent_recorder);
    }


    /**
     * When service is restarted, register Receivers and start updating of graphs
     */
    public void onRestart() {
        super.onRestart();

        // register Receivers to gain data
        if (toAllReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_ACTIVITY);
            registerReceiver(toAllReceiver, intentFilter);
        }
        if(demoDataReceiver != null) {
            IntentFilter intentFilter = new IntentFilter(ACTION_STRING_MEASURING);
            registerReceiver(demoDataReceiver, intentFilter);
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        super.onStart();
    }


}