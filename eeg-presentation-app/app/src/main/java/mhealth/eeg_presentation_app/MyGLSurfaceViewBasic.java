package mhealth.eeg_presentation_app;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;

/**
 * Created by Ina on 22/03/16.
 */
public class MyGLSurfaceViewBasic extends GLSurfaceView {
    private final MyGLRenderer mRenderer;
    public MyGLSurfaceViewBasic(Context context, MyGLRenderer renderer) {
        super(context);

        // Create an OpenGL ES 1.0 context
        setEGLContextClientVersion(1);

        mRenderer = renderer;

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(renderer);
    }

    public MyGLRenderer getRenderer() {
        return mRenderer;
    }
    public MyGLSurfaceViewBasic(Context context, AttributeSet attrs,MyGLRenderer renderer) {
        super(context, attrs);
        // Create an OpenGL ES 1.0 context
        setEGLContextClientVersion(1);

        mRenderer = renderer;

        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(mRenderer);
    }

}
